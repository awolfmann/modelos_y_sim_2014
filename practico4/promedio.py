# Calculo Promedios grandes 
import math
import random

def experimento(n, k, fun):
    suma = 0.0
    for i in range(k):
        u = random.random()
        x = int(n * u) + 1
        suma = suma + (fun(x)/k)
    return suma
