import math
import random

def experimento(n):
    hits = 0
    for i in range(n):
        u = random.random()
        j = int(u * n) + 1
        if j == i :
            hits += 1
    return hits


def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento(n)

    prom = s/n
    return prom

def var(n):
    #estimador varianza, varianza muestral
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento(n) ** 2.0 

    var = (s / n) - (esp(n) * 2.0)
    return var    


print var(10000)
