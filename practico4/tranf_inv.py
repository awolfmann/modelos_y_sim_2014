# Hacer transformada inversa directamente
import math
import random

def fun(x):
    return (0.5**(x+1)) + (2.0**(x-2.0) / (3.0**x))
      
def experimento():
    F = fun(1.0)
    i = 0.0 
    u = random.random()
    print "u ", u
    print "F ", F
    while u > F:
        print "entro!"
        i += 1.0
        F += fun(i)

    return i

print experimento()
