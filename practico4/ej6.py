# Metodo de la tasa discreta de riesgo

import math
import random


def lamda(x):
    suma = 0.0
    for i in range(x-1):
        suma += p(i)

    return p(x) / (1.0 - suma)


def experimento():
    x = 1
    u = random.random()
    while u >= lamda(x): 
        x += 1 
        u = random.random()

    return x
