import random
import math

N = 10**4


def func_a(i):
    return math.exp(i / float(N))


def aproximation(k):
    total_sum = 0
    Xi = 0
    for i in xrange(k):
        Xi = int(N*random.random()) + 1
        total_sum += func_a(Xi)
    return N * (total_sum / float(k))


# Funcion original que quiero aproximar, sirve para comparar resultados
def func2():
    sum2 = 0.0
    for i in range(10000):
        sum2 += math.exp(i / float(10000))
    return sum2
