import math
import random

def experimento(n, lamda, k):
    suma = 0.0
    for j in range (k):
        v = (math.pow(lamda, j) / math.factorial(j)) * math.exp(-lamda)
        suma += v

    u = random.random()
    i = 0.0
    p = math.exp(-lamda) / suma
    F = p
    while u >= F :
        p = ((lamda * p) / (i + 1)) / suma
        F += p
        i += 1

    return i

#Hacer una lista de los pi
# Sea f(i, k, lamda)
ps = [f(i, k, lamda for i in range(k + 1)]
M = sum(ps)
P = [p_i / M for p_i in ps]
P[i]


#A continuación se establecen ciertos principios para poder elegir un algoritmo para generar
#variables aleatorias:
#1. Si la función es invertible, usar transformada inversa.
#2. Si la función no es invertible, el método de rechazo o el método de caracterización para
#distribuciones normales o de Poisson.
#3. Los métodos de convolución y composición se usan para casos especiales.

