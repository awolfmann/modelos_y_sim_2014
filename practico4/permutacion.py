import math
import random

def experimento(p, n):
    k = n
    while k > 1:
        u = random.random()
        i = int(k * u) + 1
        tmp = p[i]
        p[i] = p[k]
        p[k] = tmp
        k -= 1

