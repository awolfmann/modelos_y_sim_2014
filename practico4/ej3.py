import math
import random

def experimento():
    lista = [2 , 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    count = 0
    
    while len(lista) > 0:
        u1 = random.random()
        u2 = random.random()
        d1 = int (u1 * 6) + 1
        d2 = int (u2 * 6) + 1
        suma = d1 + d2 
        try:    
            lista.remove(suma)
        except ValueError:
            pass
        count += 1
    
    return count

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento()

    prom = s/n
    return prom


def var(n):
    #estimador varianza, varianza muestral
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento() ** 2.0

    var = (s / n) - (esp(n) ** 2.0)
    return var
   
def desvio(n):
    return pow(var(n), 0.5)

print "en 100 esperanza %f, desvio %f " % (esp(100), desvio(100)) 
print "en 1000 esperanza %f, desvio %f " % (esp(1000), desvio(1000))
print "en 10000 esperanza %f, desvio %f " % (esp(10000), desvio(10000))
print "en 100000 esperanza %f, desvio %f " % (esp(100000), desvio(100000))
