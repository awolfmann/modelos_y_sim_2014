import math
import random

def experimento(l):
    s = 0.0
    u = random.random()
    while u > (lamda(s) / l):
        y = int(math.log(u) / math.log(1.0-l)) + 1.0
        s += y
    x = s
    
    return x
