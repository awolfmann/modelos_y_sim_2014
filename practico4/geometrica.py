# Generacion v.a. geometrica, parametro p
import math
import random

def experimento(p):
    # p: probabilidad
    
    v = random.random()
    q = 1.0 - p
    
    x = int (math.log(v) / math.log(q)) + 1.0

    return x     
