# Generacion v.a. poisson parametro lamda

import math
import random

def experimento(lamda):

    u = random.random()
    i = 0.0
    p = math.exp(-lamda)
    F = p
    while u >= F :
        p = (lamda * p) / (i + 1)
        F += p
        i += 1

    return i

