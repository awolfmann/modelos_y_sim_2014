# Calculo Promedios grandes 
import math
import random

def experimento(n):
    suma = 0.0
    for i in range(n):
        suma = suma + math.exp(i / n)
    return suma

print "experimento ", experimento(1000)

def fun(x):
    return math.exp(x/1000)
    
def aprox(k):
    suma = 0.0
    for i in range(k):
        u = random.random()
        x = int(1000 * u) + 1
        suma = suma + fun(x)
    return 1000 * (suma / float(k))

print "aprox ", aprox(100)

# en la aproximacion deberia ser suma * 10?
