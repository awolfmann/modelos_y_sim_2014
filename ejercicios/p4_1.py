# Hacer transformada inversa directamente
import math
import random

def experimento(n):
    count = 0.0
    
    for i in range(n):
        u = random.random()
        if u < (1.0/3.0):
            x = 1
            count += 1.0
        
    return count / n

print experimento(10000)
