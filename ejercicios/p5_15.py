# Rechazo de una gamma con una exponencial 
import math
import random

def experimento():
    y = random.random()
    fy = 30*(y**2 - 2* y**3 + y**4)
    gy = 1
    c = 15.0 / 8.0
    count = 1
    u = random.random()
    while u > fy / (c * gy): 
        y = random.random()
        fy = 30*(y**2 - 2* y**3 + y**4)

        count += 1

    return y

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento()

    prom = s/n
    return prom

print esp(1000000)
