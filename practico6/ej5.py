import math
import random

def estimate_pi(n):
    count = 0
    for i in range(n):
        x = random.uniform(-1.0,1.0)
        y = random.uniform(-1.0,1.0)
        d = (x ** 2.0 + y ** 2.0) ** 0.5
        if d <= 1.0:
            count += 1 

    return (float(count)/ float(n)) * 4.0

print estimate_pi(1000)


def fun():
    xi = random.uniform(-1.0,1.0)
    yi = random.uniform(-1.0,1.0)
    d = (xi ** 2.0 + yi ** 2.0) ** 0.5
    if d <= 1.0:
        x = 4.0
    else:
        x = 0.0
    return x

def experimento(f, d):
    n = 0
    mean = 0.0
    M2 = 0.0
    r = float('inf')
    l = - float('inf')
    while n < 30 or math.fabs(r-l) > d:
        n = n + 1.0
        x = f()
        delta = x - mean
        mean += delta / n
        M2 += delta * (x - mean)
        if n > 1:
            var = M2 / (n - 1.0)
            std = var ** 0.5
            l = mean - (1.96 * std) / (n ** 0.5)
            r = mean + (1.96 * std) / (n ** 0.5)

    return n, mean, l, r, var, std 
n, mean, l, r,var, std  = experimento(fun, 0.1)
var_e = var / float(n)
print "cantidad de ejecuciones", n
print "esperanza muestral: ", mean
print "varianza muestral: ", var
print "desvio std muestral: ", std
print "varianza del estimador: ", var_e
print "intervalo de confianza de 95% ", (l, r)
