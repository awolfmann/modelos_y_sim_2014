import math
import random

# var(S2) = E[(S2 - E[S2])**2] = sum n**n de (Si**2 - S**2)**2 / n**n
# aprox bootstrap
# var(S2) = E[(S2 - E[S2])**2] = E[(S2 - Var(X))**2]
# Varianza muestral var(x) = s2 = (sum(xi - x_bar) ** 2) / (n -1),  E[s2] = sigma2
# media muestral x_bar = sum(xi) / n, E[x_bar]=tita 
# Aprox bootstrap ECM(X_bar, mu)
# ECM(X_bar, mu) = E[(X_bar - mu)**2] = 1 / n**2 sum hasta n(xi - mu)**2

def experimento(data, var):
    n = len(data)
    mu = sum(data) / float(n)
    s2 = 0.0
    for d in data:
        s2 += (d - mu) ** 2.0
    s2 /= float(n -1)
    return (s2 - var) ** 2.0

def simulate_bootstrap(r, data):
    n = len(data)
    mu = sum(data) / float(n)
    var = 0.0
    for d in data:
        var += (d - mu) ** 2.0
    var /= float(n)
    res = 0.0
    for _ in range(r):
        sample = []
        for i in range(n):
            I = int(random.random() * n)
            sample.append(data[I])
        res += experimento(sample, var)
            
    return float(res) / r

xs = [1, 3]
print 'simulacion bootstrap del p-valor', simulate_bootstrap(r=4, data=xs)
# Inciso a.
# El metodo bootstrap, para estimar p, se puede utilizar, tomando la muestra inicial y apartir de ahi
# simular las r muestras con reposicion