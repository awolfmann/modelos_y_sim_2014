import math
import random
from scipy.misc import factorial
# math.factorial da overflow cuando es mayor a 170


def estimate_e(k):
    suma = 0.0
    for i in range(k):
        suma += 1.0 / factorial(i)

    return suma
print 'estimacion de e', estimate_e(1000)

def experimento1(n):
    x = []

    for i in range(n):
        u1 = random.random()
        u2 = random.random()
        count = 2
        while u1 < u2:
            #mientras que el predecesor es menor que el actual, sigo contando
            u1 = u2
            u2 = random.random()
            count += 1
    
        x.append(count)

    return x


def online_variance(data):
    n = 0
    mean = 0.0
    M2 = 0.0
 
    for x in data:
        n = n + 1
        delta = x - mean
        mean = mean + delta/n
        M2 = M2 + delta*(x - mean)
 
    variance = M2/(n - 1)
    return mean, variance

xs = experimento1(1000)
e, v = online_variance(xs)
d = v ** 0.5
var_e = v / 1000.0
# intervalo de 95%, z0.025 = 1.96
l = e - 1.96 * d / (1000 ** 0.5)
r = e + 1.96 * d / (1000 ** 0.5)

print "esperanza muestral: ", e
print "varianza muestral: ", v
print "desvio std muestral: ", d
print "varianza del estimador: ", var_e
print "intervalo de confianza de 95% ", (l, r)
