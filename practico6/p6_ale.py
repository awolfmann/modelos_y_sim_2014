import math
import numpy as np
from random import random

def foo(j0, d, f):
    s2 = 0.0
    M = f()
    for j in range(2, j0 + 1):
        x = f()
        A = M
        M = M + (x - M) / j
        s2 = (1 - 1.0 / (j - 1)) * s2 + j * (M - A) ** 2
    j = j0
    if d != float("inf"):
        while (math.sqrt(s2 / j)) > d:
            j = j + 1
            x = f()
            A = M
            M = M + (x - M) / j
            s2 = (1 - 1.0 / (j - 1)) * s2 + j * (M - A) ** 2
    return [j, M, s2]

def baz(d, z, f):
    M = f()
    s2 = 0.0
    for j in range(2, 31):
        x = f()
        A = M
        M = M + (x - M) / j
        s2 = (1 - 1.0 / (j - 1)) * s2 + j * (M - A) ** 2
    j = 30.0
    while 2 * z * math.sqrt(s2 / j) > d:
        j = j + 1
        x = f()
        A = M
        M = M + (x - M) / j
        s2 = (1 - 1.0 / (j - 1)) * s2 + j * (M - A) ** 2
    return [j, M, s2]

def bar():
    iters = 10 ** 3
    aux = 0.0
    for _ in range(iters):
        f = np.random.standard_normal
        aux += foo(30, 0.1, f)[0]
    return aux / iters

print "\n1)"
print "a) %d" % bar()
[j, M, s2] = foo(30, 0.1, np.random.standard_normal)
print "b) %f" % j
print "c) %f" % M
print "d) %f" % s2
print "e) No son sorprendentes, pues se muestro N(0,1), luego c ~ 0 y d ~ 1\n"

print "2)"
f = lambda: math.exp(random() ** 2)
[j, M, s2] = foo(100, 0.01, f)
print "Se generaron %d valores" % j
print "El valor estimado de la integral es %f" % M
print "Varianza %f\n" % s2

print "3)"
def experiment():
    c = 0.0
    s = 0.0
    while True:
        c += 1
        s += random()
        if s > 1:
            break
    return c
[j, M, s2] = foo(1000, float("inf"), experiment)
print "Media     %f" % M
print "Varianza  %f" % s2
delta = 1.96 * math.sqrt(s2 / 1000.0)
print "Intervalo [%f, %f]\n" % (M - delta , M + delta)

print "4)"
def experiment():
    old = random()
    new = random()
    c = 2.0
    while old <= new:
        old = new
        new = random()
        c += 1
    return c
[j, M, s2] = foo(1000, float("inf"), experiment)
print "Media     %f" % M
print "Varianza  %f" % s2
delta = 1.96 * math.sqrt(s2 / 1000.0)
print "Intervalo [%f, %f]\n" % (M - delta , M + delta)

print "5)"
def experiment():
    x = 2 * random() - 1
    y = 2 * random() - 1
    if x ** 2 + y ** 2 <= 1:
        res = 4
    else:
        res = 0
    return res
[j, M, s2] = baz(0.1, 1.96, experiment)
print "Ejecuciones %d" % j
print "Media       %f" % M
print "Varianza    %f" % s2
delta = 1.96 * math.sqrt(s2 / j)
print "Intervalo [%f, %f]\n" % (M - delta , M + delta)

print "6)"
b = 5
a = -5
count = 0
iters = 100000
data = [56, 101, 78, 67, 93, 87, 64, 72, 80, 69]
mean = lambda data: float(sum(data)) / len(data)
u = mean(data)
for _ in range(iters):
    sample = []
    for _ in range(10):
        I = int(random() * 10)
        sample.append(data[I])
    mean_ = mean(sample)
    if -5 < (mean_ - u) and (mean_ - u) < 5:
        count += 1
print "El valor estimado para p es %f\n" % (float(count) / iters)

print "7)"
print "La estimacion bootstrap de Var(S**2) es 1\n"

print "8)"
def server():
    l1 = 4.0
    l2 = 4.2
    t = 0
    i = 0
    repairs = {}
    arrivals = {}
    time_on_system = 0.0
    more_arrivals = True
    exp = lambda x: (-1.0 / x) * math.log(random())
    while True:
        new_arrival = t + exp(l1)
        if repairs:
            min_key = min(repairs, key=repairs.get)
            repair_time = repairs[min_key]
        else:
            repair_time = float("inf")
        if more_arrivals and new_arrival < repair_time:
            if t + new_arrival > 8:
                more_arrivals = False
            if more_arrivals and len(repairs) <= 3:
                i += 1
                t += new_arrival
                arrivals[i] = t
                repairs[i] = t + exp(l2)
        else:
            t = repair_time
            repairs.pop(min_key)
            time_on_system += repair_time - arrivals[min_key]
        if not more_arrivals and not repairs:
            break
    return [time_on_system, i]
B = 100
iters = 10000
ww = []
nn = []
for i in range(iters):
    [w, n] = server()
    ww.append(w)
    nn.append(n)
d_ = np.mean(ww)
n_ = np.mean(nn)
mean = d_ / n_
print "Tiempo promedio que un cliente pasa en el sistema %f" % mean
ecm = 0.0
for _ in range(B):
    d = 0.0
    n = 0.0
    for _ in range(iters):
        I = int(random() * iters)
        d += ww[I]
        n += nn[I]
    ecm += (d / n - mean) ** 2
ecm /= B
print "El error cuadratico medio del estimador es %f\n" % ecm
