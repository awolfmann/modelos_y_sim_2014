import math
import random

def fun1(x):
    return math.exp(x ** 2.0)

def fun2():
    xs = []
    while True:
        x = fun1(random.random())
        xs.append(x)
        a = sum(xs) / len(xs)
        yield a

def montecarlo(N, d, fun):
    n = 0
    mean = 0.0
    M2 = 0.0
    std = 0.0
    estimation = fun()

    while n < N or std > d:
        x = next(estimation)
        n += 1
        delta = x - mean
        mean += delta / n
        M2 += delta * (x - mean)
        if n > 1:
            variance = M2 / (n - 1)
            std = variance ** 0.5

    return mean, n

print "estimacion: ", montecarlo(N=100, d=0.01, fun=fun2 )



