import math
import numpy as np


def experimento():
    x = np.random.normal(size=30)
    s = x.std(ddof=1)

    while s / (len(x) ** 0.5) >= 0.1:
        xi = np.random.normal()
        x = np.append(x, xi)
        s = x.std(ddof=1)

    return x

x = experimento()
print "Cantidad de datos generados: ", len(x)
print "Media muestral: ", np.mean(x)
print "Varianza muestral: ", x.var(ddof=1)



def esperanza(iters, lista):
    '''
    Calcula el valor promedio de una lista, es un estimador de la esperanza muestral 
    '''
    s = 0.0
    for i in range(iters):
        s += lista[i]

    return float(s / iters)

def desvio(iters, lista, e):
    '''
    Calcula el desvio estandar muestral, sobre los valores de una lista
    '''
    d = 0.0
    for i in range(iters):
        d += (lista[i] - e) ** 2.0

    return float(d / (iters-1)) ** 0.5



