import math
import random


def experimento(a, b, xs, mu):
    count = False
    n = len(xs)
    suma = 0.0 
    for i in range(n):
        suma += xs[i] 
    suma = suma / float(n) - mu
    if suma > a and suma < b:
        count = True
    return count

def simulate_bootstrap(r, data, a, b):
    n = len(data)
    mu = sum(data) / float(n)
    count = 0
    for _ in range(r):
        sample = []
        for i in range(n):
            I = int(random.random() * n)
            sample.append(data[I])
        if experimento(a, b, sample, mu):
            count +=1

    return float(count) / r

xs = [56, 101, 78, 67, 93, 87, 64, 72, 80, 69]
print 'simulacion bootstrap del p-valor', simulate_bootstrap(r=10000, data=xs, a=-5.0, b=5.0)
# Inciso a.
# El metodo bootstrap, para estimar p, se puede utilizar, tomando la muestra inicial y apartir de ahi
# simular las r muestras con reposicion