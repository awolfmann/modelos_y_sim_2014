import math
import random


def min_sum():
    count = 0.0
    s = 0.0
    while s < 1:
        val = random.random()
        count += 1
        s += val

    return count


def experimento(iters):
    x = []
    for i in range(iters):
        xi = min_sum()
        x.append(xi)

    return x

def online_variance(data):
    n = 0
    mean = 0
    M2 = 0
 
    for x in data:
        n = n + 1
        delta = x - mean
        mean = mean + delta/n
        M2 = M2 + delta*(x - mean)
 
    variance = M2/(n - 1)
    return mean, variance


xs = experimento(1000)
e, v = online_variance(xs)
d = v ** 0.5
var_e = v / 1000.0
# intervalo de 95%, z0.025 = 1.96
l = e - 1.96 * d / (1000 ** 0.5)
r = e + 1.96 * d / (1000 ** 0.5)

print "esperanza muestral: ", e
print "varianza muestral: ", v
print "desvio std muestral: ", d
print "varianza del estimador: ", var_e
print "intervalo de confianza de 95% ", (l, r)

# Varianza muestral s2 = (sum(xi - x_bar) ** 2) / n -1,  E[s2] = sigma2
# media muestral x_bar = sum(xi) / n, E[x_bar]=tita
# Varianza de x_bar = s2 / n
 

