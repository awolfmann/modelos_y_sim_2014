import math
import random

def media_estimate(n, d, fun):
    '''
    Estimacion de la media M de X, con error d
    '''
    x = fun()
    m = x
    s2 = 0.0
    for j in range(2, n + 1):
        x = fun()
        a = m
        m += (x-m) / j
        s2 = 1.0 - 1.0 / (j - 1.0) * s2 + j * (m - a) ** 2.0
    j = n
    while (s2 / j) ** 0.5 > d:
        j += 1
        x = fun()
        a = m
        m += (x-m) / j
        s2 = 1.0 - 1.0 / (j - 1.0) * s2 + j * (m - a) ** 2.0

    return m 

def bernoulli():
    ''' Genera variable aleatoria Bernoulli'''
    u = random.random()
    if u > 0.5:
        x = 1.0
    else:
        x = 0.0

    return x

def prop_estimate(d):
    '''Estimacion de la probabilidad p de X con error d'''
    x = bernoulli()
    p = x
    for j in range(1, 101):
        x = bernoulli()
        p += (x - p) / j

    j = 100
    while (p * (1 - p) / j) ** 0.5 > d:
        j += 1
        x = bernoulli()
        p += (x - p) / j

    return p

def online_mean(N, d, fun):
    n = 0
    mean = 0
    M2 = 0
 
    while n < N or std > d:
        x = fun(random.random())
        n += 1
        delta = x - mean
        mean += delta / n
        M2 += delta * (x - mean)
        variance = M2 / (n - 1)
        std = variance ** 0.5

    return mean
