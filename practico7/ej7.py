import math
import random
from scipy.stats import expon


def estimate_lamda(xs):
	s = sum(xs) / len(xs)
	return 1.0 / s

def acum_exp(xs, lamda):
	prob = []
	for i in range(len(xs)):
		prob.append(expon.cdf(xs[i], scale = 1.0 / lamda))

	return prob

def d_valor(prob):
    d = []
    n = len(prob)
    for i in xrange(n):
        d.append(((i + 1) / float(n)) - prob[i])
        d.append(prob[i] - (i / float(n)))

    return max(d)

def simulate_D(n):
	u = []
	D = []
	for i in range(n):
		ui=random.random()
		u.append(ui)
	u.sort()
	for i in range(n):
		D.append(((i + 1) / float(n)) - u[i])
		D.append(u[i] - (i / float(n)))

	return max(D) 


def k_s(r, xs, acum):
	lamda = estimate_lamda(xs)
	ac_xs = acum(xs, lamda)
	d = d_valor(ac_xs)
	count = 0.0
	for i in range(r):
		D = simulate_D(len(xs))
		if D >= d:
	 		count += 1.0


 	p_valor = float(count) / r

 	return d, p_valor

xs = [1.6, 10.3, 3.5, 13.5, 18.4, 7.7, 24.3, 10.7, 8.4, 4.9, 7.9, 12.0, 16.2, 6.8, 14.7]
xs.sort()
print k_s(100000, xs, acum_exp)