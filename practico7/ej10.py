from scipy.stats import mannwhitneyu


from ej9 import aprox_normal, simulation_aprox, sum_range
from e9_pablo import rec_p_valor

ys1 = [19, 36, 44, 49, 52, 72, 72]
xs = [19, 31, 39, 45, 47, 66, 75]
ys = [28, 36, 44, 49, 52, 72, 72]
r = sum_range(xs,ys)

print "aprox normal al p_valor", aprox_normal(xs, ys)
print "aprox por simulacion, en 10000 iteraciones", simulation_aprox(xs, ys, 10000)
print "p_valor exacto", rec_p_valor(N=len(xs), M=len(ys), T=r)
print "scipy", mannwhitneyu(xs, ys)

# Use only when the number of observation in each sample is > 20 and you have 2 independent samples 
# of ranks. Mann-Whitney U is significant if the u-obtained is LESS THAN 
# or equal to the critical value of U.

# This test corrects for ties and by default uses a continuity correction. 
# The reported p-value is for a one-sided hypothesis, to get the two-sided p-value multiply 
# the returned p-value by 2.





