scipy.stats.ks_2samp(data1, data2)[source]
Computes the Kolmogorov-Smirnov statistic on 2 samples.

This is a two-sided test for the null hypothesis that 2 independent samples are drawn from the same continuous distribution.

Parameters:	
a, b : sequence of 1-D ndarrays
two arrays of sample observations assumed to be drawn from a continuous distribution, sample sizes can be different
Returns:	
D : float
KS statistic
p-value : float
two-tailed p-value
Notes

This tests whether 2 samples are drawn from the same distribution. Note that, like in the case of the one-sample K-S test, the distribution is assumed to be continuous.

This is the two-sided test, one-sided tests are not implemented. The test uses the two-sided asymptotic Kolmogorov-Smirnov distribution.

If the K-S statistic is small or the p-value is high, then we cannot reject the hypothesis that the distributions of the two samples are the same.

--------------------------------------------------------------------------------
scipy.stats.kstest(rvs, cdf, args=(), N=20, alternative='two-sided', mode='approx')[source]
Perform the Kolmogorov-Smirnov test for goodness of fit.

This performs a test of the distribution G(x) of an observed random variable against a given distribution F(x). Under the null hypothesis the two distributions are identical, G(x)=F(x). The alternative hypothesis can be either ‘two-sided’ (default), ‘less’ or ‘greater’. The KS test is only valid for continuous distributions.

Parameters:	
rvs : str, array or callable
If a string, it should be the name of a distribution in scipy.stats. If an array, it should be a 1-D array of observations of random variables. If a callable, it should be a function to generate random variables; it is required to have a keyword argument size.
cdf : str or callable
If a string, it should be the name of a distribution in scipy.stats. If rvs is a string then cdf can be False or the same as rvs. If a callable, that callable is used to calculate the cdf.
args : tuple, sequence, optional
Distribution parameters, used if rvs or cdf are strings.
N : int, optional
Sample size if rvs is string or callable. Default is 20.
alternative : {‘two-sided’, ‘less’,’greater’}, optional
Defines the alternative hypothesis (see explanation above). Default is ‘two-sided’.
mode : ‘approx’ (default) or ‘asymp’, optional
Defines the distribution used for calculating the p-value.
‘approx’ : use approximation to exact distribution of test statistic
‘asymp’ : use asymptotic distribution of test statistic
Returns:	
D : float
KS test statistic, either D, D+ or D-.
p-value : float
One-tailed or two-tailed p-value.
Notes

In the one-sided test, the alternative is that the empirical cumulative distribution function of the random variable is “less” or “greater” than the cumulative distribution function F(x) of the hypothesis, G(x)<=F(x), resp. G(x)>=F(x).

-------------------------------------------------------------------------------
scipy.stats.chisquare(f_obs, f_exp=None, ddof=0, axis=0)
Calculates a one-way chi square test.

The chi square test tests the null hypothesis that the categorical data has the given frequencies.

Parameters:	
f_obs : array_like
Observed frequencies in each category.
f_exp : array_like, optional
Expected frequencies in each category. By default the categories are assumed to be equally likely.
ddof : int, optional
“Delta degrees of freedom”: adjustment to the degrees of freedom for the p-value. The p-value is computed using a chi-squared distribution with k - 1 - ddof degrees of freedom, where k is the number of observed frequencies. The default value of ddof is 0.
axis : int or None, optional
The axis of the broadcast result of f_obs and f_exp along which to apply the test. If axis is None, all values in f_obs are treated as a single data set. Default is 0.
Returns:	
chisq : float or ndarray
The chi-squared test statistic. The value is a float if axis is None or f_obs and f_exp are 1-D.
p : float or ndarray
The p-value of the test. The value is a float if ddof and the return value chisq are scalars.

Notes

This test is invalid when the observed or expected frequencies in each category are too small. A typical rule is that all of the observed and expected frequencies should be at least 5.

The default degrees of freedom, k-1, are for the case when no parameters of the distribution are estimated. If p parameters are estimated by efficient maximum likelihood then the correct degrees of freedom are k-1-p. If the parameters are estimated in a different way, then the dof can be between k-1-p and k-1. However, it is also possible that the asymptotic distribution is not a chisquare, in which case this test is not appropriate.
---------------------------------------------------------------------------------
scipy.stats.ranksums(x, y)
Compute the Wilcoxon rank-sum statistic for two samples.

The Wilcoxon rank-sum test tests the null hypothesis that two sets of measurements are drawn from the same distribution. The alternative hypothesis is that values in one sample are more likely to be larger than the values in the other sample.

This test should be used to compare two samples from continuous distributions. It does not handle ties between measurements in x and y. For tie-handling and an optional continuity correction see scipy.stats.mannwhitneyu.

Parameters:	
x,y : array_like
The data from the two samples
Returns:	
z-statistic : float
The test statistic under the large-sample approximation that the rank sum statistic is normally distributed
p-value : float
The two-sided p-value of the test


--------------------------------------------------------------------------------
scipy.stats.kruskal

Compute the Kruskal-Wallis H-test for independent samples

The Kruskal-Wallis H-test tests the null hypothesis that the population median of all of the groups are equal. It is a non-parametric version of ANOVA. The test works on 2 or more independent samples, which may have different sizes. Note that rejecting the null hypothesis does not indicate which of the groups differs. Post-hoc comparisons between groups are required to determine which groups are different.

Parameters:	
sample1, sample2, ... : array_like
Two or more arrays with the sample measurements can be given as arguments.
Returns:	
H-statistic : float
The Kruskal-Wallis H statistic, corrected for ties
p-value : float
The p-value for the test using the assumption that H has a chi square distribution

Notes
Due to the assumption that H has a chi square distribution, the number of samples in each group must not be too small. A typical rule is that each sample must have at least 5 measurements.



