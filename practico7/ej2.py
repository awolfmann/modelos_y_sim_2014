import math
import random
from scipy.stats import chi2


def generate_va(n):
    xs = []
    for i in range(n):
        u = random.random()
        if u < 1/6.0:
            xi = 1
        elif u < 2/6.0:
            xi = 2
        elif u < 3/6.0:
            xi = 3
        elif u < 4/6.0:
            xi = 4
        elif u < 5/6.0:
            xi = 5
        else:
            xi = 6

        xs.append(xi)

    return xs

def f_obs(xs, k):
    N = [0] * k
    for x in xs:
        if x == 1:
            N[0] += 1
        elif x == 2:
            N[1] += 1
        elif x == 3:
            N[2] += 1
        elif x == 4:
            N[3] += 1
        elif x == 5:
            N[4] += 1
        else:
            N[5] += 1

    return N

def f_esp():
    P = [1.0/6.0] * 6

    return P

def estadistico_t(N, P, n):
    suma = 0.0
    for i in range(len(N)):
        suma += (N[i] - (n * P[i])) ** 2.0 / (n * P[i])
    return suma

def aprox_chi():
    N = [158, 172, 164, 181, 160, 165]
    P = [1/6.0] * 6
    t = estadistico_t(N,P, 1000)
    p = 1.0 - chi2.cdf(t, 5)
    return t, p

print "aproximacion chi2", aprox_chi()

def simulate_t(r, n, k, t):
    T = []
    for i in range(r):
        xs = generate_va(n)
        N = f_obs(xs, k)
        P = f_esp()
        T.append(estadistico_t(N, P, n))

    count = 0
    for x in T:
        if x >= t:
            count += 1

    return float(count) / r

print simulate_t(r=100, n=1000, k=6, t=2.18)
