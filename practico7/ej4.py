import math
import random
from scipy.stats import chi2, expon

xs = [86, 133, 75, 22, 11, 144, 78, 122, 8, 146, 33, 41, 99]

def f_esp(k):
	P = []
	for i in range(1,k+1):
		P.append((expon.cdf(i, 0.02)- expon.cdf(i-1, 0.02)))
	P[k-1] += (1.0 - sum(P))
	return P

def f_obs(xs, k):
    N = [0] * k
    for x in xs:
        N[int(x / 10.0)] += 1

    return N

def estadistico_t(N, P, n):
    suma = 0.0
    for i in range(len(N)):
        suma += (N[i] - n*P[i]) ** 2.0 / (n*P[i])
    return suma

def aprox_chi(xs, k):
    N = f_obs(xs, k)
    P = f_esp(k)
    t = estadistico_t(N,P, len(xs))
    p = 1.0 - chi2.cdf(t, k-1)
    return t,p
print aprox_chi(xs , 15)
