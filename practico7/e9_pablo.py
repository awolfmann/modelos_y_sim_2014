# coding=utf-8
import math
import random

import ej9 

comma = ','
N0 = '0'
M0 = '0'

xs = [65.2, 67.1, 69.4, 78.4, 74.0, 80.3]
ys = [59.4, 72.1, 68.0, 66.2, 58.5]
r = ej9.sum_range(xs,ys)

# Calculo recursivo del p-valor, N tamaño de la primer muestra
# M tamaño de la segunda, T rango de la primer muestra
def rec_p_valor(N, M, T):
    P = {}  # Diccionario para almacenar las recursiones
    # Inicializo el diccionario, todo en 0
    for i in range(N + 1):
        for j in range(M + 1):
            for k in range(T + 1):
                key = str(i) + comma + str(j) + comma + str(k)  # P['i,j,k']
                P[key] = 0
    for i in range(1, N + 1):
        a = i * (i + 1) / 2
        for k in range(a, T + 1):
            key = str(i) + comma + M0 + comma + str(k)  # P['i,0,k']
            P[key] = 1
    for k in range(1, T + 2):
        for j in range(1, M + 1):
            key = N0 + comma + str(j) + comma + str(k - 1)  # P['0,j,k']
            P[key] = 1
    for i in range(1, N + 1):
        for j in range(1, M + 1):
            for k in range(1, T + 1):
                key1 = str(i) + comma + str(j) + comma + str(k)
                key2 = str(i) + comma + str(j - 1) + comma + str(k)
                if k < (i + j):
                    P[key1] = (j / float(i + j)) * P[key2]
                else:
                    key3 = str(i - 1) + comma + str(j) + comma + str(k - i - j)
                    P[key1] = (i / float(i + j)) * P[key3] +\
                              (j / float(i + j)) * P[key2]
    key1 = str(N) + comma + str(M) + comma + str(T)
    key2 = str(N) + comma + str(M) + comma + str(T - 1)
    V = min(P[key1], 1 - P[key2])  # Min(P_n,m(r), 1 - P_n,m(r - 1))
    return 2 * V
#print "p_valor exacto", rec_p_valor(N=len(xs), M=len(ys), T=r)