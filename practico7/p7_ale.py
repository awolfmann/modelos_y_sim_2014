import math
from scipy import stats
import numpy as np
from random import random
from collections import Counter

ITERS = 10 ** 4

class InverseTransform():
    def __init__(self, values_with_probs):
        if math.fsum([k[1] for k in values_with_probs]) != 1:
            raise ValueError("Probabilites should sum 1")
        self.values_with_probs = sorted(
            values_with_probs, key=lambda x: x[1], reverse=True)

    def random(self):
        u = random()
        F = 0.0
        for val in self.values_with_probs:
            F += val[1]
            if u < F:
                return val[0]

def T(n, k, N, p):
    t = 0.0
    for i in p.keys():
        t += (N[i] - n * p[i]) ** 2 / (n * p[i])
    return t

def generate_sample(p, n):
    values = []
    it = InverseTransform(p)
    for _ in range(n):
        values.append(it.random())
    return values

def dict_to_array(d):
    a = []
    for key, value in p.iteritems():
        temp = [key,value]
        a.append(temp)
    return a

def p_value(n, k, N, p):
    """
    Si T suma k elementos entonces el p_valor teorico es
    P(chi2_(k-1) >= t) para el t en la muestra. Si se estimaron
    k parametros es P(chi2_(k-1-m) >= t)
    """
    iters = ITERS
    p_value = 0.0
    t = T(n, k, N, p)
    print "El valor de t es %f" % t
    vp = dict_to_array(p)
    for _ in range(iters):
        sample = generate_sample(vp, n)
        tt = T(n, k, Counter(sample), p)
        p_value += 1 if t <= tt else 0
    p_value /= iters
    return p_value

def ji_squared(n, k, N, p):
    pvalue = p_value(n, k, N, p)
    if pvalue < 0.05:
        print "Hay evidencia suficiente para rechazar "\
            "la hipotesis nula en favor de la hipotersis "\
            "alternativa"
    else:
        print "No hay evidencia suficiente para rechazar "\
            "la hipotesis nula en favor de la hipotersis "\
            "alternativa"

def D(n, F, y):
    d = -float("inf")
    for j in range(1, n + 1):
        val1 = j / float(n) - F(y[j - 1])
        val2 = F(y[j - 1]) - (j - 1) / float(n)
        d = max(val1, val2, d)
    return d

def p_value_ks(n, F, y):
    p_value = 0.0
    iters = ITERS
    d = D(n, F, sorted(y))
    print "El valor de D es %f" % d
    for _ in range(iters):
        y = sorted([random() for _ in range(n)])
        dd = D(n, lambda x: x, y)
        p_value += 1 if d <= dd else 0
    p_value /= iters
    return p_value

def komolgorov_smirnov(n, F, y):
    pvalue = p_value_ks(n, F, y)
    if pvalue < 0.05:
        print "Hay evidencia suficiente para rechazar "\
            "la hipotesis nula en favor de la hipotersis "\
            "alternativa"
    else:
        print "No hay evidencia suficiente para rechazar "\
            "la hipotesis nula en favor de la hipotersis "\
            "alternativa"

def bin(n, p, k):
    return (math.factorial(n) / (math.factorial(n - k) * math.factorial(k)))\
        * (p ** k) * ((1 - p) ** (n - k))

def p_value_optimized(n, k, N, p, pp):
    iters = ITERS
    p_value = 0.0
    t = T(n, k, N, p)
    print "El valor de t es %f" % t
    pp = [[k, bin(8, pp, k)] for k in range(9)]
    for _ in range(iters):
        sample = generate_sample(pp, n)
        p = {k: bin(8, np.mean(sample) / 8.0, k) for k in range(9)}
        tt = T(n, k, Counter(sample), p)
        p_value += 1 if t <= tt else 0
    return p_value / iters

k = 5
n = 50
N = {1: 12, 2: 5, 3: 19, 4: 7, 5: 7}
p = {1: 0.2, 2: 0.2, 3: 0.2, 4: 0.2, 5: 0.2}
print "\nDel Libro"
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value(n, k, N, p)
print "Con SciPy"
print stats.chisquare(np.array([12, 5, 19, 7, 7]), np.array([10, 10, 10, 10, 10]))
print

k = 3
n = 564
N = {1: 141, 2: 291, 3: 132}
p = {1: 1.0 / 4.0, 2: 1.0 / 2.0, 3: 1.0 / 4.0}
print "1)"
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value(n, k, N, p)
print "Con SciPy"
print stats.chisquare(np.array([141, 291, 132]), np.array([0.25 * 564, 0.5 * 564, 0.25 * 564]))
print

k = 6
n = 1000
N = {1: 158, 2: 172, 3: 164,
     4: 181, 5: 160, 6: 165}
p = {1: 1.0 / 6, 2: 1.0 / 6, 3: 1.0 / 6,
     4: 1.0 / 6, 5: 1.0 / 6, 6: 1.0 / 6}
print "2)"
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value(n, k, N, p)
print "Con SciPy"
print stats.chisquare(
    np.array([158, 172, 164, 181, 160, 165]),
    np.array([1000.0 / 6 for _ in range(6)]))
print

print "Del libro"
F = lambda x: 1 - math.exp(-x / 100.0)
n = 10
y = [66, 72, 81, 94, 112, 116, 124, 140, 145, 155]
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value_ks(n, F, y)
print "Con SciPy"
print stats.kstest(y, 'expon', args=(0, 100.0))
print

print "3)"
F = lambda x: x
n = 10
y = [0.12, 0.18, 0.06, 0.33, 0.72, 0.83, 0.36, 0.27, 0.77, 0.74]
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value_ks(n, F, y)
print "Con SciPy"
print stats.kstest(y, F)
print

print "4)"
F = lambda x: 1 - math.exp(-x / 50.0)
n = 13
y = [86, 133, 75, 22, 11, 144, 78, 122, 8, 146, 33, 41, 99]
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value_ks(n, F, y)
print "Con SciPy"
print stats.kstest(y, 'expon', args=(0, 50.0))
print

print "5)"
k = 9
n = 18
N = Counter([6, 7, 3, 4, 7, 3, 7, 2, 6, 3, 7, 8, 2, 1, 3, 5, 8, 7])
pp = np.mean([6, 7, 3, 4, 7, 3, 7, 2, 6, 3, 7, 8, 2, 1, 3, 5, 8, 7]) / 8
p = {k: bin(8, pp, k) for k in range(9)}
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value(n, k, N, p)
print "El p-valor de la muestra aproximado por "\
    "simulacion optimizada es %f\n" % p_value_optimized(n, k, N, p, pp)
print "Con SciPy"
print stats.chisquare(
    np.array([0, 1, 2, 4, 1, 1, 2, 5, 2]),
    np.array([(bin(8, pp, k) * n) for k in range(9)]))
print

print "6)"
F = lambda x: 1 - math.exp(-x)
n = 10
y = []
for _ in range(10):
    y.append(math.log(random()) * -1.0)
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value_ks(n, F, y)
print "Con SciPy"
print stats.kstest(y, 'expon', args=(0, 1.0))
print

print "7)"
n = 15
y = [1.6, 10.3, 3.5, 13.5, 18.4, 7.7, 24.3, 10.7, 8.4,
     4.9, 7.9, 12, 16.2, 6.8, 14.7]
l = 1 / np.mean(y)
F = lambda x: 1 - math.exp(-x * l)
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value_ks(n, F, y)
print "Con SciPy"
print stats.kstest(y, 'expon', args=(0, 1 / l))
print

print "8)"
n = 12
y = [91.9, 97.8, 111.4, 122.3, 105.4, 95.0, 103.8, 99.6, 96.6, 119.3, 104.8, 101.7]
u = np.mean(y)
sigma = np.std(y)
F = lambda x: stats.norm.cdf((x - u) / sigma)
print "El p-valor de la muestra aproximado por "\
    "simulacion es %f\n" % p_value_ks(n, F, y)
print "Con SciPy"
print stats.kstest(y, 'norm', args=(u, sigma))
