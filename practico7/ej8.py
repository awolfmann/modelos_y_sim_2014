import math
import random
from scipy.stats import norm


def estimate_normal_mean(sample):
    return sum(sample) / len(sample)


def estimate_normal_var(sample, e):
	sample1 = []
	for i in range(len(sample)):
		sample1.append((sample[i] - e) ** 2.0)

	return sum(sample1) / len(sample1)

def acum_normal(xs):
	prob = []
	mu = estimate_normal_mean(xs)
	sigma = estimate_normal_var(xs, mu) ** 0.5
	for i in range(len(xs)):
		prob.append(norm.cdf(xs[i], mu, sigma))

	return prob

def d_valor(prob):
    d = []
    n = len(prob)
    for i in xrange(n):
        d.append(((i + 1) / float(n)) - prob[i])
        d.append(prob[i] - (i / float(n)))

    return max(d)

def simulate_D(n):
	u = []
	D = []
	for i in range(n):
		ui=random.random()
		u.append(ui)
	u.sort()
	for i in range(n):
		D.append(((i + 1) / float(n)) - u[i])
		D.append(u[i] - (i / float(n)))

	return max(D) 


def k_s(r, xs, acum):
	ac_xs = acum(xs)
	d = d_valor(ac_xs)
	count = 0.0
	for i in range(r):
		D = simulate_D(len(xs))
		if D >= d:
	 		count += 1.0


 	p_valor = float(count) / r

 	return d, p_valor

xs = [91.9, 97.8, 111.4, 122.3, 105.4, 95.0, 103.8, 99.6, 96.6, 119.3, 104.8, 101.7]
xs.sort()
print k_s(100000, xs, acum_normal)