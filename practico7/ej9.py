import math
import random
from scipy.stats import norm


def sum_range(xs,ys):
	zs = xs + ys
	zs.sort()
	R = 0
	for i in range(len(zs)):
		if zs[i] in xs:
			R += i + 1
	return R

def mann_whitney(xs, ys):
	r = sum_range(xs, ys)
	n_xs = len(xs)
	n_ys = len(ys)
	u = (n_xs * n_ys) + ((n_xs * (n_xs + 1)) / 2.0 ) - r
	return u

xs = [65.2, 67.1, 69.4, 78.4, 74.0, 80.3]
ys = [59.4, 72.1, 68.0, 66.2, 58.5]
r = sum_range(xs,ys)
#print mann_whitney(xs, ys)


def aprox_normal(xs, ys):
	n = len(xs)
	m = len(ys)
	r = sum_range(xs, ys)
	r_mean = n * (n + m + 1)/ 2.0
	r_var = n * m * (n + m + 1)/ 12.0
	z = (r - r_mean) / (r_var ** 0.5)

	if r <= n * (n + m + 1) / 2.0:
		p_valor = 2 * norm.cdf(z)
	else: 
		p_valor = 2.0 * (1 - norm.cdf(z))

	return p_valor

print "aprox normal al p_valor", aprox_normal(xs, ys)


def simulation_aprox(xs, ys, k):
	r = sum_range(xs, ys)
	n = len(xs)
	m = len(ys)
	rs = []
	for i in range(k):
		ri = sum(random.sample(range(1, n + m + 1), n))
		rs.append(ri)

	count1 = 0
	count2 = 0
	for ri in rs:
		if ri >= r:
			count1 += 1
		else:
			count2 += 1

	p1 = float(count1) / k
	p2 = float(count2) / k
	p_value = 2 * (min(p1, p2))

	return p_value
 
print "aprox por simulacion, en 10000 iteraciones", simulation_aprox(xs, ys, 10000)

def recursive_P(n,m,r):
	if n == 1 and m == 0:
		if r <= 0.0:
			return 0
		else:
			return 1
	elif n == 0 and m == 1:
		if r < 0.0:
			return 0
		else:
			return 1
	elif n < 0 or m < 0:
		return 0
	else:
		a = (n / float(n + m)) * recursive_P(n-1, m, r-n-m)
		b = (m / float(n + m)) * recursive_P(n, m-1, r)
		return a + b



print recursive_P(n=len(xs), m=len(ys), r=r)
