

def proc_poisson(ns):
	n = len(ns)
	media = sum(ns) / float(n)
	var = 0.0
	for i in range(n):
		var += ((ns[i] - media) ** 2.0) / float(n - 1)

ys1 = [19, 36, 44, 49, 52, 72, 72]
xs = [19, 31, 39, 45, 47, 66, 75]
ys = [28, 36, 44, 49, 52, 72, 72]

def sum_range_rep(xs,ys):
# ver caso de que los datos repetidos esten solo en ys
	zs = xs + ys
	zs.sort()
	R = 0
	skip = False
	for i in range(len(zs)):
		if skip:
			skip = False
			continue
		else:
			if zs[i] in xs and zs[i] in ys:
				R += i + 0.5
				skip = True
				print zs[i]
				print 'entre'
			elif zs[i] in xs:
				R += i + 1 
	return R

def kruskal_wallis(xs):
	#xs es una lista de listas
	m = len(xs)
	ns = []
	for x in xs:
		ni = len(x)
		ns.append(ni)

	n = sum(ns)
	rs_means = []
	for i in range(m):
		ri_mean = ns[i] * (n + 1.0) / 2.0	
		rs_means.append(ri_mean)

	suma = 0.0
	for i in range(m):
		suma += ((rs_means[i] - (ns[i] * (n + 1.0)) / 2.0) ** 2.0) / ns[i]

	r = 12.0 / (n * (n + 1.0)) * suma

	return r
	# valor p = P(R>= r)
	# si los tam muestra son grandes, R puede aprox por chi2 con m-1 df
	# puede usarse simulacion
	# la aprox chi2 puede utilizarse si hay datos repetidos 