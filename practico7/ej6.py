
import math
import random
from scipy.stats import expon

#Generar v.a. Exponencial, parametro lamda
def gen_exp(lamda):
    u = random.random()
    x = - (1.0/lamda) * math.log(u)
    return x

def experimento():
	xs = []
	for i in range(10):
		xi = gen_exp(1.0)
		xs.append(xi)
	return xs

def acum_exp(xs, lamda):
	prob = []
	for i in range(len(xs)):
		prob.append(expon.cdf(xs[i], scale = 1.0 / lamda))

	return prob

def d_valor(prob):
    d = []
    n = len(prob)
    for i in xrange(n):
        d.append(((i + 1) / float(n)) - prob[i])
        d.append(prob[i] - (i / float(n)))

    return max(d)

def simulate_D(n):
	u = []
	D = []
	for i in range(n):
		ui=random.random()
		u.append(ui)
	u.sort()
	for i in range(n):
		D.append(((i + 1) / float(n)) - u[i])
		D.append(u[i] - (i / float(n)))

	return max(D) 


def k_s(r, xs, acum):
	d = d_valor(acum)
	count = 0.0
	for i in range(r):
		D = simulate_D(len(xs))
		if D >= d:
	 		count += 1.0


 	p_valor = float(count) / r

 	return d, p_valor

xs = experimento()
xs.sort()
print k_s(100000, xs, acum_exp(xs, 1.0))