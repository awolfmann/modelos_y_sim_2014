import math
import random
from scipy.stats import chi2, binom

xs = [6, 7, 3, 4, 7, 3, 7, 2, 6, 3, 7, 8, 2, 1, 3, 5, 8, 7]
n = 8

def f_obs(xs, k):
    N = [0] * (k + 1)
    for x in xs:
        N[x] += 1
    N = N[1:] 
    return N

def fun(x, n, p):
    f = math.factorial(n) / float(math.factorial(x) * math.factorial(n - x))
    q = (p ** x) * ((1.0 - p) ** (n - x))

    return f * q



def f_esp(k, n, p):
    P = []
    for i in range(1, k+1):
        x = fun(i, n, p)
        P.append(x)
    return P

x_bar = sum(xs) /float(len(xs))
p_est = float(x_bar) / n
print "x-barra", x_bar
print 'p estimado',p_est
print 'f_obs', f_obs(xs,8)
print 'f_esp', f_esp(8, 8, p_est)


def estadistico_t(N, P, n):
    suma = 0.0
    for i in range(len(N)):
        suma += (N[i] - (n* P[i])) ** 2.0 / (n * P[i])
    return suma

def aprox_chi(xs):
    N = f_obs(xs, 8)
    P = f_esp(8, 8, p_est)
    t = estadistico_t(N,P, len(xs))
    p = 1.0 - chi2.cdf(t, 6)
    return t,p

print aprox_chi(xs)
