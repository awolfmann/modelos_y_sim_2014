import math
import random
from scipy.stats import chi2



def f_obs(xs, k):
    N = [0] * k
    for x in xs:
        N[int(x)] += 1
    return N

xs = [1.2, 1.8, 0.6, 3.3, 7.2, 8.3, 3.6, 2.7, 7.7, 7.4 ]

def estadistico_t(N, P):
    suma = 0.0
    for i in range(len(N)):
        suma += (N[i] - P[i]) ** 2.0 / P[i]
    return suma

def aprox_chi(xs):
    N = f_obs(xs, 10)
    P = [1] * 10
    t = estadistico_t(N,P)
    p = 1.0 - chi2.cdf(t, 9)
    return t,p
print aprox_chi(xs)
