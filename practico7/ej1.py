import math
import random
from scipy.stats import chi2

# si p_value > 0.05 No hay evidencia suficiente para rechazar H0
# si 0.01 < p_value < 0.05 Hay cierta evidencia para rechazar H0
# si < 0.01 Hay fuerte evidencia para rechazar H0


def generate_va(n):
    xs = []
    for i in range(n):
        u = random.random()
        if u < 0.5:
            xi = 2
        elif u < 0.75:
            xi = 1
        else:
            xi = 3
        xs.append(xi)

    return xs

def f_obs(xs, k):
    N = [0] * k
    for x in xs:
        if x == 1:
            N[0] += 1
        elif x == 2:
            N[1] += 1
        else:
            N[2] += 1
    return N

def f_esp():

    P = [1/4.0, 1/2.0, 1/4.0]

    return P

def estadistico_t(N, P, n):
    suma = 0.0
    for i in range(len(N)):
        suma += (N[i] - n * P[i]) ** 2.0 / (n *P[i])
    return suma


def simulate_t(r, n, k, t):
    T = []
    for i in range(r):
        xs = generate_va(n)
        N = f_obs(xs, k)
        P = f_esp()
        T.append(estadistico_t(N, P, n))

    count = 0
    for x in T:
        if x >= t:
            count += 1

    return float(count) / r
N = [141, 291, 132]
P = f_esp()
t = estadistico_t(N, P, 564)
# df = cant intervalos - 1
p_value = 1.0 - chi2.cdf(t, df=2)
print "aproximacion chi2", p_value
print "aprox del p-valor por simulacion",simulate_t(r=10000, n=564, k=3, t=0.8572)
