import math
import random


## Preguntar math.pow(x, 2.0) en lugar de x**2.0

def fun_a(x):
    return (1.0 - x ** 2.0) ** (3.0 / 2.0)


def fun_b(x):
    return x * (1.0 + x ** 2.0) ** (-2.0)


def fun_c(x):
    return math.exp(-x ** 2.0)


def fun_d(var):
    return math.exp((var[0] + var[1]) ** 2.0)


def fun_e(var):
    return math.exp(-(var[0] + var[1]))


def fun_f(x):
    return x * math.log(math.sin(x))

def fun_g(x):
    return ((math.sin(x)) ** 4.0) / (x ** 4.0)


def sumatoria(N, H):
    suma = 0
    for i in range(N):
        Ui = random.random()
        Hui = H(Ui)
        suma += Hui

    return suma


def montecarlo(N, G, a, b, par=False):

    resultado_sum = 0.0

    # Integral entre 0 y 1
    if a == 0.0 and b == 1.0: 
        H = G
        resultado_sum = sumatoria(N, H)

    # Integral entre a y b    
    elif a != -float('inf') and b!= float('inf'):
        H = lambda x: G(a + (b - a) * x) * (b - a)
        resultado_sum = sumatoria(N, H)

    # Integral entre 0 e INF    
    elif a == 0.0 and b == float('inf'):
        H = lambda x: G(1.0/x - 1.0)/(x**2.0)
        resultado_sum = sumatoria(N, H)

    # Integral entre -INF e INF    
    elif a == -float('inf') and b == float('inf'):
        if par:
            H = lambda x: G(1.0/x - 1.0)/(x**2.0) 
            resultado_sum = 2.0 * sumatoria(N, H) # VER si tengo que pasar N o N/2
        else:
            # VER COMO CAMBIAR LA ZONA -INF A 0
            H = lambda x: G(1.0/x - 1.0)/(x**2.0)
            H2 = lambda x: G(1.0/x - 1.0)/(x**2.0)
            resultado_sum = sumatoria(N, H) + sumatoria(N, H2)
 
    return resultado_sum/N

print "la aproximacion da", montecarlo(N=1000000, G=fun_g, a=0.0, b=float('inf'))

