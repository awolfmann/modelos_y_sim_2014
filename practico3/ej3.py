import random

def min_sum():
    count = 0.0
    s = 0.0
    while s < 1:
        val = random.random()
        count = count + 1
        s = s + val

    return count

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += min_sum()

    prom = s/n
    print prom
    return prom

esp(10000)
