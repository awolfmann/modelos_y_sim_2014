import math
import random

def experimento():
    count = 0.0
    p = 1.0
    while p >= math.exp(-3):
        val = random.random()
        count = count + 1
        p = p * val

    return count

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento()

    prom = s/n
    print prom
    return prom

esp(10000)
