import random
import math

def buffon(N):
    count = 0.0

    for i in range(N):
        angle = math.pi * random.random()
        center = 2 * random.random()
        ya = center + math.sin(angle) / 2.0
        yb = center - math.sin(angle) / 2.0

        if yb <= 0.0 or ya >= 2.0:
            count += 1.0

    return (count/float(N)) ** (-1)

print "buffon",buffon(10000)
