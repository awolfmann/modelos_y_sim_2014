#include <stdint.h>

uint32_t x=521288629, y=362436069, z=16163801, c=1, n=1131199209;
uint32_t mzran13()
{ int32_t s;
  if (y>x+c) {s=y-(x+c); c=0;}
  else { s=y-(x+c)-18; c=1; }
  x=y; y=z;
  return (z=s) + (n=69069*n+1013904243);
};
void ran13set(uint32_t xx, uint32_t yy, uint32_t zz, long nn)
  { x=xx; y=yy; z=zz; n=nn; c=y>z; }

*********************************************************************
NOTA:
La misma biblioteca incluye la constante
{UINT32_MAX} Maximum value of type unsigned long int de 32 bits

necesaria para normalizar los valores de mzran13
en el intervalo (0,1).

*********************************************************************
Implementación portable para sysop de 64 bits by
Nicolás Wolovick y Dionisio E Alonso
*********************************************************************

