import math
import random


## Preguntar math.pow(x, 2.0) en lugar de x**2.0

def fun_a(x):
    return (1.0 - x ** 2.0) ** (3.0 / 2.0)


def fun_b(x):
    return x * (1.0 + x ** 2.0) ** (-2.0)


def fun_c(x):
    return math.exp(-math.pow(x, 2.0))


def fun_d(x , y):
    return math.exp((x + y) ** 2.0)


def fun_e(x):
    return math.exp(-x) * (1.0 - math.exp(-x))


def fun_f(x):
    return x * math.log(math.sin(x))


def fun_g(x):
    return ((math.sin(x)) ** 4.0) / (x ** 4.0)


def fun_h(x):
    return math.pow(x, 2.0) * math.exp(-math.pow(x, 2.0))


def fun_i(x):
    return math.log(x) / (1 - x)


def sumatoria(N, H, nvar):
    suma = 0

    for i in range(N):
        U = []        
        for j in range(nvar):
            U.append(random.random())
        Hui = H(*U)
        #print "Hui" , Hui
        suma += Hui
        #print "suma", suma
    return suma


def montecarlo(N, G, a, b, par=False, nvar=1):

    resultado_sum = 0.0

    # Integral entre 0 y 1
    if a == 0.0 and b == 1.0:
        H = G
        resultado_sum = sumatoria(N, H, nvar)

    # Integral entre a y b
    elif a != -float('inf') and b!= float('inf'):
        H = lambda x: G( a + (b - a) * x) * (b - a)
        resultado_sum = sumatoria(N, H, nvar)

    # Integral entre 0 e INF
    elif a == 0.0 and b == float('inf'):
        H = lambda x: G(1.0/x - 1.0)/(x**2.0)
        resultado_sum = sumatoria(N, H, nvar)

    # Integral entre -INF e INF
    elif a == -float('inf') and b == float('inf'):
        if par: # if (G1 = lambda x: G(-x)) == G :
            H = lambda x: G(1.0/x - 1.0)/(x**2.0) 
            resultado_sum = 2.0 * sumatoria(N, H, nvar) # VER si tengo que pasar N o N/2
        else:
            # VER COMO CAMBIAR LA ZONA -INF A 0 
            H = lambda x: -G(x)
#            if G == H: 
#                pass # si es impar la integral da 0
            H2 = lambda x: G(1.0/x - 1.0)/(x**2.0)
            resultado_sum = sumatoria(N, H, nvar) + sumatoria(N, H2, nvar)

    return resultado_sum/N

print "la aproximacion da", montecarlo(N=1000000, G=fun_e, a=0, b=float('inf'))




