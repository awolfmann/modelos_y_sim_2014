import math
import random
from scipy.stats import norm

import ej1
import ej3

def calculate_d(xs, acum):
	F = []
	D = []
	for i in range(len(xs)):
		j = (i + 1) / len(xs)
		D.append(max((j - acum[i]), (acum[i] - i / len(xs))))
		
	return max(D)

def d_valor(prob):
    d = []
    n = len(prob)
    for i in xrange(n):
        d.append(((i + 1) / float(n)) - prob[i])
        d.append(prob[i] - (i / float(n)))

    return max(d)

def simulate_D(n):
	u = []
	D = []
	for i in range(n):
		ui=random.random()
		u.append(ui)
	u.sort()
	for i in range(n):
		D.append(((i + 1) / float(n)) - u[i])
		D.append(u[i] - (i / float(n)))

	return max(D) 


def k_s(r, xs, acum):
	d = d_valor(acum)
	count = 0.0
	for i in range(r):
		D = simulate_D(len(xs))
		if D >= d:
	 		count += 1.0
 	p_valor = float(count / r)

 	return d, p_valor 