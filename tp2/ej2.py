import math
import random

def geometrica(p):
    v = random.random()
    q = 1.0 - p

    x = int (math.log(v) / math.log(q)) + 1.0

    return x

def online_variance(data):
    n = 0
    mean = 0
    M2 = 0
 
    for x in data:
        n = n + 1
        delta = x - mean
        mean = mean + delta/n
        M2 = M2 + delta*(x - mean)
 
    variance = M2/(n - 1)
    return mean, variance

def experimento(iters, p):
    xs = []
    for i in range(iters):
        xs.append(geometrica(p))

    return xs


print "En 100 iters", online_variance(experimento(100, 0.4))
print "media muestral en 1000 iters", online_variance(experimento(1000, 0.4))
print "media muestral en 10000 iters", online_variance(experimento(10000, 0.4))

