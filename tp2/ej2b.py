import random
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import geom 

import ej2

x0 = ej2.experimento(100, 0.4)
x1 = ej2.experimento(1000, 0.4)
x2 = ej2.experimento(10000, 0.4)


n, bins, patches = plt.hist( [x0,x1,x2], 20, normed=1, range=(0, 20),                             	
                            label=['100', '1000', '10000'], histtype='step', align='left')
print bins


y = geom.pmf(bins, 0.4)
plt.plot(bins, y, label="geometrica")
plt.xlim(0,15)
plt.xlabel('Valores')
plt.ylabel('Frecuencia normalizada')
plt.title(r'Comparacion')
plt.legend()
plt.grid(True)
plt.show()