# coding=utf-8
import math
import numpy
import matplotlib.pyplot as plt
import random
import scipy.stats as st
from e1 import data
from e3 import normal_estimation, lognormal_estimation, gamma_estimation


def plot_gan(muestra):
    plt.hist(muestra, bins=400, range=(0, 400), rwidth=0.8, normed=True,
             label='-')
    plt.title('-')
    plt.xlabel('-')
    plt.ylabel('-')
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.show()


def plot_comp_normal(muestra):
    norm_vars = normal_estimation(muestra)
    u = norm_vars[0]
    o = norm_vars[2]
    x = numpy.linspace(345, 380, 36)
    norm_data = []
    for i in x:
        norm_data.append(st.norm.cdf(i + 1, u, o) - st.norm.cdf(i, u, o))
    plt.bar(x, height=norm_data, alpha=0.5, width=0.5, color='b',
            label='Distribucion Normal')
    plt.hist(muestra, bins=35, range=(345, 380), normed=True, color='r',
             width=0.5, alpha=0.6,label='Muestra')
    plt.title('-')
    plt.xlabel('-')
    plt.ylabel('-')
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.show()


def plot_comp_lognormal(muestra):
    lognorm_vars = lognormal_estimation(muestra)
    u = lognorm_vars[0]
    o = lognorm_vars[2]
    x = numpy.linspace(345, 380, 36)
    lognorm_data = []
    for i in x:
        lognorm_data.append(st.lognorm.cdf(i + 1, scale=math.exp(u), s=o) -\
                            st.lognorm.cdf(i, scale=math.exp(u), s=o))
    plt.bar(x, height=lognorm_data, alpha=0.5, width=0.5, color='b',
            label='Distribucion Lognormal')
    plt.hist(muestra, bins=35, range=(345, 380), normed=True, color='r',
             width=0.5, alpha=0.6,label='Muestra')
    plt.title('-')
    plt.xlabel('-')
    plt.ylabel('-')
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.show()



def plot_comp_gamma(muestra):
    gamma_vars = gamma_estimation(muestra)
    alpha = gamma_vars[0]
    lambda_e = gamma_vars[1]
    print alpha
    print lambda_e
    x = numpy.linspace(345, 380, 36)
    gamma_data = []
    for i in x:
        gamma_data.append(st.gamma.cdf(x=i + 1, a=alpha, scale=1/lambda_e) -\
                          st.gamma.cdf(x=i, a=alpha, scale=1/lambda_e))
    plt.bar(x, height=gamma_data, alpha=0.5, width=0.5, color='b',
            label='Distribucion Gamma')
    plt.hist(muestra, bins=35, range=(345, 380), normed=True, color='r',
             width=0.5, alpha=0.6, label='Muestra')
    plt.title('-')
    plt.xlabel('-')
    plt.ylabel('-')
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.show()