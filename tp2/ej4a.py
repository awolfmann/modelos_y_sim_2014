import math
import random
import matplotlib.pyplot as plt
import numpy as np
from pylab import *

import ej1

x = ej1.parse2()


bin_size = 1; min_edge = 345; max_edge = 379
N = (max_edge-min_edge)/bin_size; Nplus1 = N + 1
bin_list = np.linspace(min_edge, max_edge, Nplus1)
n, bins, patches = plt.hist(
    x, bins=bin_list, range=(345, 379),
    color='blue', label="Muestra", normed=True)

plt.xlabel('Valores en Pesos')
plt.ylabel('Frecuencia normalizada')
plt.title(r'Histograma de Datos Muestrales de la Ganancia')
plt.legend()
plt.grid(True)
plt.show()