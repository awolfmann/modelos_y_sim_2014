# encoding: utf-8
import pylab

def parse1():
	f1 = open('ganancia_media.dat', 'r')
	x = []
	for line in f1:
	    line = float(line.strip())
	    x.append(line)
	f1.close()

	return x


def parse2():
	f2 = open('ganancia-ins-500.dat', 'r')
	y = []
	for line in f2:
	    line = float(line.strip())
	    y.append(line)
	f2.close()

	return y

#x = parse1()
# y = parse2()
#print "min(x)", min(x)
#print "max(x)", max(x)
# print "min(y)", min(y)
#print "max(y)", max(y)

