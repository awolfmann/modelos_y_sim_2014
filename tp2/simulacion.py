import math
import random

def costo(x):
    return (x + 1.0) / x


def generate_demand(p):
    v = random.random()
    q = 1.0 - p

    x = int (math.log(v) / math.log(q)) + 1.0

    return x


def generate_time(t, lamda):
    u = random.random()
    return t - (1 / lamda) * math.log(u)


def simulate(r, lamda, d, s, S, L, h, T):
    '''
    Realiza una simulacion de un modelo de inventario
    r: ganancia por articulo
    lamda: razon de llegada de los clientes en Hs(proceso de poisson homogeneo)
    d: parametro de la distribucion de la demanda
    s: Cantidad minima del inventario
    S: Cantidad maxima del inventario
    L : demora del proveedor en entregar pedido(en Hs)
    h: costo de mantenimiento por hora por articulo
    T: tiempo de funcionamiento del sistema, en Hs
    '''

    #Inicializacion
    t = 0.0 #Variable de tiempo
    x = 0.0 # Cantidad de inventario disponible (Var Estado)
    y = 0.0 # Cantidad Solicitada (Var Estado)
    C = 0.0 # Cant total de costos de los pedidos hasta t
    H = 0.0 # Cant total de costos de mantenimient de inventario hasta t
    R = 0.0 # Cant total de ingresos obtenidos hasta t
    t0 = 0.0 # tiempo del evento llegada del siguiente cliente
    t0 = generate_time(t, lamda) # Genero tiempo llegada primer cliente
    t1 = float('inf') # Tiempo del evento entrega de un pedido, 
                        # si no hay pedido pendiente, es inf

    while t < T:
        # Caso 1 llego un cliente antes que llegue un pedido
        if t0 < t1:
            H += (t0-t) * x * h # entre t y t0, hay un costo de mantenim
            t = t0
            D = generate_demand(d) # Demanda, v.a. distribucion G
            w = min(D, x) # Cantidad del pedido a cubrir
            R += w * r # Incrementa ganancia
            x -= w # disminuye el inventario
            if x < s and y == 0: # hay menos stock que el minimo y no hay pedido
                y = S - x
                t1 = t + L

            t0 = generate_time(t, lamda)

        # Caso 2 llego un pedido antes que el siguiente cliente
        else:
            H += (t1 - t) * x * h
            t = t1
            C += costo(y)
            x += y
            y = 0.0
            t1 = float('inf')

    return R

print simulate(r=2.0, lamda=20.0, d=0.4, s=5, S=50, L=5.0, h=0.1, T=40.0)
