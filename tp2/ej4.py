import math
import random
import matplotlib.pyplot as plt
import ej1

x = ej1.parse2()

n, bins, patches = plt.hist(
    x, bins=35, range=(345, 379),
    color='blue', label="datos", normed=True)

plt.xlabel('valores')
plt.ylabel('Frecuencia normalizada')
plt.title(r'Histograma ')
plt.legend()
plt.show()