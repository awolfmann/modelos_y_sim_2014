import math
import random
from scipy.stats import *
import numpy as np

import kolmogrov
import ej1
import ej3 

xs = ej1.parse1()

def acum_normal(xs):
	prob = []
	mu = ej3.estimate_normal_mean(xs)
	sigma = ej3.estimate_normal_var(xs, mu) ** 0.5
	for i in range(len(xs)):
		prob.append(norm.cdf(xs[i], mu, sigma))

	return prob

xs.sort()
print "normal", kolmogrov.k_s(10000, xs, acum_normal(xs))

