# coding=utf-8
import math
import random


def d_valor(prob, n):
    prob_sup = []
    n = len(prob)
    for i in xrange(n):
        prob_sup.append(math.fabs(((i + 1) / float(n)) - prob[i]))
        prob_sup.append(math.fabs(prob[i] - (i / float(n))))
    return max(prob_sup)


def simulation(d, n):
    prob = [random.random() for _ in xrange(n)]
    prob.sort()
    D = d_valor(prob, n)
    return D >= d


def p_valor(d, n, r):
    p = 0
    for i in xrange(r):
        p += simulation(d, n)
    return p / float(r)