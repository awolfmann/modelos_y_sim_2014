import math
import random

import ej1


def estimate_normal_mean(sample):
    return sum(sample) / len(sample)


def estimate_normal_var(sample, e):
	sample1 = []
	for i in range(len(sample)):
		sample1.append((sample[i] - e) ** 2.0)

	return sum(sample1) / len(sample1)


def estimate_lognormal_mean(sample):
	sample1 = []
	sample1 = sample
	sample1 = map(math.log, sample1)
	return sum(sample1) / len(sample1)


def estimate_lognormal_var(sample, e):
	sample1 = []
	for i in range(len(sample)):
		sample1.append((math.log(sample[i]) - e) ** 2.0)
	return sum(sample1) / len(sample1)


def estimate_gamma_alfa(sample):
	a = math.log(estimate_normal_mean(sample)) - estimate_lognormal_mean(sample)
	alfa = (1.0 / (4.0 * a)) * (1.0 + (1.0 + (4.0/3.0) * a)**0.5)
	return alfa

def estimate_gamma_lambda(sample, alfa):
	return alfa / estimate_normal_mean(sample)


xs = ej1.parse2()
e = estimate_normal_mean(xs)
print "media normal", e
print "var normal", estimate_normal_var(xs, e)**0.5
e1 = estimate_lognormal_mean(xs)
print "media lognorm", e1
print "var lognorm", estimate_lognormal_var(xs, e1)**0.5
a = estimate_gamma_alfa(xs)
print "alfa gamma", a
print "lamda gamma", estimate_gamma_lambda(xs, a)