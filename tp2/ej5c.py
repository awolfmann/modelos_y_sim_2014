import random
import math
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm

import ej3
import ej1

fig, ax = plt.subplots(1)

xs = ej1.parse1()

n, bins, patches = plt.hist(
    xs, bins=16, range=(365, 366.5),
    color='blue', label="Ganancias Medias", normed=True)

mu = ej3.estimate_normal_mean(xs)
sigma = (ej3.estimate_normal_var(xs, mu))**0.5

y = norm.pdf(bins, mu, sigma)
ax.plot(bins, y, color='red', label='Ajuste Normal', lw=2.0)


textstr = '$\^\mu=%.4f$\n$\^\sigma=%.4f$'%(mu, sigma)
# these are matplotlib.patch.Patch properties
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
# place a text box in upper left in axes coords
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
ax.set_xlabel('Valores en Pesos')
ax.set_ylabel('Frecuencia normalizada')
ax.set_title(r'Histograma de Comparacion Ganancias Medias con Aprox. normal')
ax.legend()
ax.grid(True)
plt.show()
