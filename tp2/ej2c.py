import random
import numpy as np
import pylab as P
from scipy.stats import chisquare, geom

import ej2


def f_obs(xs,k):
	N = [0] * (k+1)

	for x in xs:
		for i in range(1, k+1):
			if x>=i and x<i+1:
				N[i] +=1

	N.append(len(xs) - sum(N))
	N= N[1:]
	return N


def f_esp(k):
	geom_data = []
	for i in range(1,k+1):
		geom_data.append(geom.pmf(i, 0.4))
	geom_data.append(1.0 - sum(geom_data))

	return geom_data

def estadistico(xs, k, N, P):
	suma = 0.0
	#N = f_obs(xs, k)
	n = len(xs)
	#P = f_esp(k)
	for i in range(k):
		suma += ((N[i] - n * P[i]) ** 2.0) / (n * P[i])

	return suma



xs0 = ej2.experimento(10000, 0.4)

N0 = f_obs(xs0, 18)
P0 = f_esp(18)
s0 = estadistico(xs0, 18, N0, P0)
print "estadistico0", s0
print "pvalor en 10000", chisquare(f_obs=N0, f_exp=P0, ddof=17)
xs1 = ej2.experimento(1000, 0.4)
N1 = f_obs(xs1, 18)
P1 = f_esp(18)
s1 = estadistico(xs1, 18, N1, P1)

print "estadistico1", s1
print "pvalor en 1000", chisquare(f_obs=N1, f_exp=P1, ddof=17)
xs2 = ej2.experimento(100, 0.4)
N2 = f_obs(xs2, 18)
P2 = f_esp(18)
s2 = estadistico(xs2, 18, N2, P2)
print "estadistico2", s2
print "pvalor en 10000", chisquare(f_obs=N2, f_exp= , ddof=17)

