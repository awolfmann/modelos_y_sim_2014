import math
import random
import matplotlib.pyplot as plt
import numpy as np

import ej1

x = ej1.parse1()
x_array = np.array(x)
weights = np.ones_like(x_array)/len(x_array)

n, bins, patches = plt.hist(
    x_array, bins=16, range=(365, 366.5),
    color='blue', label="datos", normed=True, weights=weights)


plt.xlabel('valores')
plt.ylabel('Frecuencia normalizada')
plt.title(r'Histograma de Ganancias Medias')
plt.legend()
plt.grid(True)
plt.show()