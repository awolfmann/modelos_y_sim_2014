import math
import random
from scipy.stats import *
import numpy as np

import kolmogrov
import ej1
import ej3 
import kspablo

#xs = np.array(ej1.parse2())
xs = ej1.parse2()


#d, pval = stats.kstest((xs-xs.mean())/xs.std(), 'norm')
#print "normal", d, pval
def acum_normal(xs):
	prob = []
	mu = ej3.estimate_normal_mean(xs)
	sigma = ej3.estimate_normal_var(xs, mu) ** 0.5
	for i in range(len(xs)):
		prob.append(norm.cdf(xs[i], mu, sigma))

	return prob


def acum_lognormal(xs):
	prob = []
	mu = ej3.estimate_lognormal_mean(xs)
	sigma = (ej3.estimate_lognormal_var(xs, mu))**0.5
	for i in range(len(xs)):
		prob.append(lognorm.cdf(xs[i], scale=math.exp(mu), s=sigma))
	return prob


def acum_gamma(xs):
	alfa = ej3.estimate_gamma_alfa(xs)
	lamda = ej3.estimate_gamma_lambda(xs, alfa)
	prob = []
	for i in range(len(xs)):
		prob.append(gamma.cdf(xs[i], a=alfa, scale=1.0/lamda))
	return prob

#print kolmogrov.calculate_d(xs, acum_normal(xs))
xs.sort()
print "normal", kolmogrov.k_s(10000, xs, acum_normal(xs))
print "lognormal", kolmogrov.k_s(10000, xs, acum_lognormal(xs))
print "gamma", kolmogrov.k_s(10000, xs, acum_gamma(xs))
