import numpy as np
import pylab as P

import ej3
import ej1

x = ej1.parse2()
mu = ej3.estimate_normal_mean(x)
sigma = ej3.estimate_normal_variance(x, mu)

n, bins, patches = P.hist(
    x, bins=35, range=(345, 379),
    color='blue', label="datos", normed=True)

# add a line showing the expected distribution
y = P.normpdf( bins, mu, sigma)
P.plot(bins, y, linewidth=1.5, color='red')

P.show()