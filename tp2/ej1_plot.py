import matplotlib.pyplot as plt

import ej1

x = ej1.parse1()
xs = []
ys = []
for i in range(len(x)-1):
	xs.append(x[i])

for i in range(1,len(x)):
	ys.append(x[i])

print "xs", len(xs)
print "ys", len(ys)

plt.scatter(xs,ys)
plt.xlabel('Xi')
plt.ylabel('Xi+1')
plt.title(r'Comparacion')
plt.grid(True)
plt.legend()
plt.show()