import random
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import geom 

import ej2

def plot_comp():


    x0 = ej2.experimento(100, 0.4)
    x1 = ej2.experimento(1000, 0.4)
    x2 = ej2.experimento(10000, 0.4)
    index = numpy.linspace(0, 10, 20)
    bar_width = 0.1
    opacity = 0.6
    labels = [str(i) for i in xrange(1, 21)]
    rects1 = plt.bar(index, prob_t, bar_width,
                    alpha=opacity,
                    color='b',
                    label='Prob. puntual teorica')
    rects2 = plt.bar(index + bar_width, x0, bar_width,
                    alpha=opacity,
                    color='r',
                    label='Prob. puntual segun la muestra, n = 100')
    rects3 = plt.bar(index + 2 * bar_width, x1, bar_width,
                    alpha=opacity,
                    color='g',
                    label='Prob. puntual segun la muestra, n = 1000')
    rects4 = plt.bar(index + 3 * bar_width, x2, bar_width,
                    alpha=opacity,
                    color='m',
                    label='Prob. puntual segun la muestra, n = 10000')
    plt.xlabel('i')
    plt.ylabel('P(X = i)')
    plt.title('Bondad de las variables aleatorias Geometricas(0.4) generadas')
    plt.xticks(index + 2*bar_width, labels)
    plt.legend(loc='upper right')
    plt.grid()
    plt.show()