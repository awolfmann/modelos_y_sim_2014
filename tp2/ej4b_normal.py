import math
import random
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import scipy.stats as st

import ej1
import ej3


xs = ej1.parse2()

def comp_normal(xs):

	bin_size = 1.0; min_edge = 345; max_edge = 379
	N = (max_edge-min_edge)/bin_size; Nplus1 = N + 1
	bin_list = np.linspace(min_edge, max_edge, Nplus1)
	n, bins, patches = plt.hist(
	    xs, bins=bin_list, range=(345, 379),
	    color='blue', label="Muestra", normed=True, alpha=0.7)

	mu = ej3.estimate_normal_mean(xs)
	sigma = (ej3.estimate_normal_var(xs, mu))**0.5
	x = np.linspace(345, 379, 36)
	norm_data = []
	for i in x:
	    norm_data.append(st.norm.cdf(i + 1, mu, sigma) - st.norm.cdf(i, mu, sigma))
	plt.bar(x, height=norm_data, alpha=0.5, color='red',
	        label='Distribucion Normal')

	plt.xlabel('Valores en Pesos')
	plt.ylabel('Frecuencia normalizada')
	plt.title(r'Histograma de Comparacion muestra con Aprox. normal')
	plt.legend()
	plt.grid(True)
	plt.show()

#comp_normal(xs)

def comp_lognormal(xs):
	bin_size = 1.0; min_edge = 345; max_edge = 379
	N = (max_edge-min_edge)/bin_size; Nplus1 = N + 1
	bin_list = np.linspace(min_edge, max_edge, Nplus1)
	n, bins, patches = plt.hist(
	    xs, bins=bin_list, range=(345, 379),
	    color='blue', label="Muestra", normed=True, alpha=0.7)

	mu = ej3.estimate_lognormal_mean(xs)
	sigma = (ej3.estimate_lognormal_var(xs, mu))**0.5
	x = np.linspace(345, 379, 36)
	lognorm_data = []
	for i in x:
		lognorm_data.append(st.lognorm.cdf(i + 1, scale=math.exp(mu), s=sigma) - 
							st.lognorm.cdf(i, scale=math.exp(mu), s=sigma))
	
	plt.bar(x, height=lognorm_data, alpha=0.5, color='red',
	        label='Distribucion Lognormal')
	plt.xlabel('Valores en Pesos')
	plt.ylabel('Frecuencia normalizada')
	plt.title(r'Histograma de Comparacion muestra con Aprox. Lognormal')
	plt.legend()
	plt.grid(True)
	plt.show()

#comp_lognormal(xs)

def comp_gamma(xs):
	bin_size = 1.0; min_edge = 345; max_edge = 379
	N = (max_edge-min_edge)/bin_size; Nplus1 = N + 1
	bin_list = np.linspace(min_edge, max_edge, Nplus1)
	n, bins, patches = plt.hist(
	    xs, bins=bin_list, range=(345, 379),
	    color='blue', label="Muestra", normed=True, alpha=0.7)

	alfa = ej3.estimate_gamma_alfa(xs)
	lamda = ej3.estimate_gamma_lambda(xs, alfa)
	x = np.linspace(345, 379, 36)
	gamma_data = []
	for i in x:
	    gamma_data.append(st.gamma.cdf(x=i + 1, a=alfa, scale=1.0/lamda) -
							st.gamma.cdf(x=i, a=alfa, scale=1.0/lamda))
	plt.bar(x, height=gamma_data, alpha=0.5, color='yellow',
			label='Distribucion Gamma')

	plt.xlabel('Valores en Pesos')
	plt.ylabel('Frecuencia normalizada')
	plt.title(r'Histograma de Comparacion muestra con Aprox. Lognormal')
	plt.legend()
	plt.grid(True)
	plt.show()

comp_gamma(xs)