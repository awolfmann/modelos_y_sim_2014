import random
import math

import ej3
import ej1

xs = ej1.parse1()
mu = ej3.estimate_normal_mean(xs)
sigma = ej3.estimate_normal_var(xs, mu)

print "mu", mu
print "sigma", sigma