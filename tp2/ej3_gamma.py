import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gamma

import ej3
import ej1

fig, ax = plt.subplots(1, 1)
x = ej1.parse2()
F = gamma.fit(x, loc=0)
alfa = ej3.estimate_gamma_alfa(x)
lamda = ej3.estimate_gamma_lambda(x, alfa)

n, bins, patches = ax.hist(
    x, bins=35, range=(345, 379),
    color='blue', label="Ganancias", normed=True)
x = np.linspace(0, 500, 1000)
#ax.plot(x, gamma.pdf(x, 36.6, scale=10),'r-', lw=5, alpha=0.6, label='gamma pdf')
ax.plot(bins, gamma.pdf(bins, alfa, scale=1.0/lamda),'r-', lw=2, label='Ajuste Gamma')
#ax.plot(bins, gamma.pdf(bins, *F),'b-', lw=5, alpha=0.6, label='gamma pdf')
ax.grid(True)
ax.legend()
ax.set_title('Ajuste Gamma a las Ganancias')
textstr = r'$\^\alpha =%.4f$ $\^\lambda=%.4f$'%(alfa, lamda)
# these are matplotlib.patch.Patch properties
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
# place a text box in upper left in axes coords
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
plt.show()