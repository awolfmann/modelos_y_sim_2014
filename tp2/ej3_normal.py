import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

import ej3
import ej1

fig, ax = plt.subplots(1)

x = ej1.parse2()
mu = ej3.estimate_normal_mean(x)
sigma = (ej3.estimate_normal_var(x, mu))**0.5


n, bins, patches = ax.hist(
    x, bins=35, range=(345, 379),
    color='blue', label="Ganancias", normed=True)

y = norm.pdf(bins, mu, sigma)
ax.plot(bins, y, color='red', label='Ajuste Normal', lw=2.0)
ax.legend()
ax.set_title('Ajuste Normal a las Ganancias')
ax.grid(True)
textstr = '$\^\mu=%.4f$\n$\^\sigma=%.4f$'%(mu, sigma)
# these are matplotlib.patch.Patch properties
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
# place a text box in upper left in axes coords
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
plt.show()