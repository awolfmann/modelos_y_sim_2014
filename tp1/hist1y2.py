import math
import random
import matplotlib.pyplot as plt
import ejercicio1

x = ejercicio1.simulate_niters(iters=10000, N=5, S=2, TR=0.125, TF=1.0)
y = ejercicio1.simulate_niters(iters=10000, N=5, S=3, TR=0.125, TF=1.0)

n, bins, patches = plt.hist(
    x, bins=50, range=(0, 15),
    color='blue', label="S=2, OP=1", normed=True)
n, bins, patches = plt.hist(
    y, bins=50, range=(0, 15),
    color='yellow', label="S=3, OP=1", alpha=0.6, normed=True)

plt.xlabel('Tiempo')
plt.ylabel('Frecuencia normalizada')
plt.title(r'Histograma de Tiempos de Falla en 10000 simulaciones')
plt.legend()
plt.show()
