# encoding: utf-8 -*-
import math
import random

def exp(lamda):
    '''
    Genera Variables aleatorias, con distribucion exponencial, de parametro lamda
    '''
    u = random.random()
    x = - (1.0/lamda) * math.log(u)

    return x

def esperanza(iters, lista):
    s = 0.0
    for i in range(iters):
        s += lista[i]

    return float(s / iters)

def desvio(iters, lista, esperanza):
    d = 0.0
    for i in range(iters):
        d += (lista[i] - esperanza) ** 2.0

    return float(d / iters) ** 0.5

def simulate_niters(iters, N, S, TR, TF, OP):
    lista = []
    for i in range(iters):
        s = Simulator(N, S, TR, TF, OP)
        xi = s.simular()
        lista.append(xi)

    return lista


class Simulator(object):
    '''
    Clase contenedora de la simulacion
    '''
    def __init__(self, N, S, TR, TF, OP):
        self.rotas = 0
        self.repuestos = S
        self.funcionan = N
        self.OP = OP
        self.oper_libres = OP
        self.t = 0.0
        self.t_reparacion = float(TR)  # lambda
        self.t_fallo = float(TF)  # lambda
        self.activo = True
        self.t_list = []
        self.niters = []


        for i in range(self.funcionan):
            x = exp(self.t_fallo)
            self.t_list.append((x, self.rompio_maquina))


        self.t_list.sort()
 

    def simular(self):
        while self.activo:
            self.t_list.sort()
            assert self.t_list[0][0] < self.t_list [1][0] # Ordenado por tiempos
            self.t, callback = self.t_list.pop(0)
            callback()
        return self.t


    def entra_servicio(self):
        ti = self.t + exp(self.t_fallo)
        self.t_list.append((ti, self.rompio_maquina))
        #print "{:<10.4f} maquina entro serv, quedan rotas {}".format(self.t, self.rotas)

    def rompio_maquina(self):
        self.rotas += 1
        #print "{:<10.4f} Se rompio una maquina, Quedan rotas {}".format(self.t, self.rotas)
        if self.rotas > self.repuestos:
            #print "{:<10.4f} No hay suficientes repuestos, sist falla".format(self.t)
            self.activo = False
            return

        self.entra_servicio()
        if self.oper_libres > 0:
            self.operario_arregla()


    def operario_arregla(self):
        assert self.rotas > 0
        assert self.oper_libres > 0  # Hay operarios libres
        assert self.oper_libres <= self.OP
        self.oper_libres -= 1
        ti = self.t + exp(self.t_reparacion)
        self.t_list.append((ti, self.maquina_reparada))
        #print "maq rep en ", ti
        #print "{:<10.4f} Operario arregla, oper libres {}".format(self.t, self.oper_libres)


    def maquina_reparada(self):
        assert self.rotas > 0  # Habia maquinas rotas
        self.rotas -= 1
        self.oper_libres += 1
        assert self.oper_libres <= self.OP
        #print "{:<10.4f} Maquina reparada. Rotas: {} Oper {}".format(self.t, self.rotas, self.oper_libres)



if __name__ == "__main__":
    l = simulate_niters(iters=100000, N=5, S=2, TR=8.0, TF=1.0, OP=2)
    e = esperanza(iters=len(l), lista=l)
    print "esperanza ", e
    ds = desvio(iters=len(l), lista=l, esperanza=e)
    print "desvio ", ds
