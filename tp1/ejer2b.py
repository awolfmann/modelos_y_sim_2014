# -*- coding: utf-8 -*-
import math
import random
import pdb

def experimento(N, S, TR, TF, OP):

    # Inicializacion
    t = 0.0  # Variable de tiempo
    r = 0  # Variable de estado, num de maquinas descompuestas en el instante t
    t_list = []  # Lista de tiempos de los eventos
    trep = [float('inf')]  # Lista de tiempos de reparacion
    oper = OP

    for i in range(N):
        u = random.random()
        x = - TF * math.log(u) # V.A. exp con media TF
        t_list.append(x)

    t_list.sort()
    #print "lista inicial", t_list

    while True:
        #assert len(trep)<=3

        # Caso 1 Se rompe maquina
        if t_list[0] < trep[0]:
            #print "entre caso 1"
            t = t_list[0]
            r += 1  # Maquina ha fallado
            #print "{:<10.4f} Se rompe maquina {}".format(t, r)
            t_list.pop(0)

            if r > S: 
                return t  # No hay mas maquinas de repuesto

            elif r <= S:
                #print "{:<10.4f} Repuesto entra en servicio".format(t)
                u = random.random()
                x = - TF * math.log(u) #Genero proxima fallo de la maquina que 
                                       # entro en funcionamiento
                t_list.append(t + x)
                t_list.sort()
          #      pdb.set_trace()

                if r <= 2: # Hay operarios desocupados
                    #print "{:<10.4f} Operario a arreglar maquina {}".format(t, r)
                    u1 = random.random()
                    y1 = - TR * math.log(u)  # V.A. exp con media TR
                    trep.append(t + y1)
                    trep.sort()
                    oper -= 1
         #           pdb.set_trace()

        # Caso 2 operario termino antes que se haya roto otra
        elif trep[0] <= t_list[0] and r >= 0:
            #print "entre caso 2"
            t = trep[0]
            r -= 1
            #print "{:<10.4f} Maquina reparada {}".format(t, r)
            oper += 1
            trep.pop(0)

            # if r - (OP - oper) > 0:  # Cantidad de maquinas que no estan siendo reparadas
            #     assert False
            #     print "{:<10.4f} Operario a arreglar maquina".format(t)
            #     u1 = random.random()
            #     y1 = - TR * math.log(u)  # V.A. exp con media TR
            #     trep.append(t + y1)
            #     trep.sort()
            #     oper -= 1
            #print "trep", trep
            #print "t_list", t_list

        #pdb.set_trace()

def simulation(iters, N, S, TR, TF, OP):
    s = 0.0
    prom = 0.0
    d = 0.0
    desv = 0.0
    for i in range(iters):
        xi = experimento(N, S, TR, TF, OP)
        s += xi
        d += xi ** 2.0
    prom = s / iters
    desv = math.sqrt((d / iters) - (prom ** 2.0))
    return {"esperanza":prom, "desvio": desv}

#print experimento(N=5, S=2, TR=0.125, TF=1.0, OP=2)
#print simulation(iters=100, N=5, S=2, TR=0.125, TF=1.0, OP=2)
#print simulation(iters=1000, N=5, S=2, TR=0.125, TF=1.0, OP=2)
#print simulation(iters=10000, N=5, S=2, TR=0.125, TF=1.0, OP=2)
print simulation(iters=100000, N=5, S=2, TR=0.125, TF=1.0, OP=2)
