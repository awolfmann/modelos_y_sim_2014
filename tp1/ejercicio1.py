# -*- coding: utf-8 -*-
import math
import random


def exp(lamda):
    '''
    Genera Variables aleatorias, con distribucion exponencial, 
    de parametro lamda
    '''
    u = random.random()
    x = - (1.0/lamda) * math.log(u)

    return x


def experimento(N, S, TR, TF):
    '''
    Realiza la simulacion del problema
    '''
    # Inicializacion
    t = 0.0  # Variable de tiempo
    r = 0  # Variable de estado, num de maquinas descompuestas en el instante t
    t_s = float('inf')  # Tiempo en que una maquina rota vuelve a funcionar
    t_list = []  # Lista de tiempos de los eventos

    for i in range(N):
        x = exp(1 / TF)
        t_list.append(x)

    t_list.append(t_s)
    t_list.sort()

    while True:

        # Caso 1 Se rompe maquina
        if t_list[0] < t_s:
            t = t_list[0]
            r += 1  # Maquina ha fallado

            if r > S: 
                return t  # No hay mas maquinas de repuesto

            elif r <= S:
                x = exp(1 / TF)  #Genero proximo tiempo de fallo
                t_list.pop(0)
                t_list.append(t + x)
                t_list.sort()

                if r == 1:
                    y = exp(1 / TR)  # Genero tiempo reparacion
                    t_s = t + y

        # Caso 2 Reparador termino
        elif t_s <= t_list[0]:
            t = t_s
            r -= 1

            if r > 0:
                y = exp(1 / TR)  # V.A. exp con media TR
                t_s = t + y

            elif r == 0:
                t_s = float('inf')


def simulate_niters(iters, N, S, TR, TF):
    '''
    Realiza iters simulaciones con parametros N, S, TR y TF, devuelve una lista con los valores de las simulaciones
    '''
    x = []
    for i in range(iters):
        xi = experimento(N, S, TR, TF)
        x.append(xi)

    return x


def esperanza(iters, lista):
    '''
    Calcula el valor promedio de una lista, es un estimador de la esperanza muestral 
    '''
    s = 0.0
    for i in range(iters):
        s += lista[i]

    return float(s / iters)


def desvio(iters, lista, esperanza):
    '''
    Calcula el desvio estandar muestral, sobre los valores de una lista
    '''
    d = 0.0
    for i in range(iters):
        d += (lista[i] - esperanza) ** 2.0

    return float(d / iters) ** 0.5


if __name__ == "__main__":
    l1 = simulate_niters(iters=100, N=5, S=2, TR=0.125, TF=1.0)
    e1 = esperanza(iters=len(l1), lista=l1)
    ds1 = desvio(iters=len(l1), lista=l1, esperanza=e1)
    print "En {} iteraciones con S=2, OP=1, Esperanza: {}, Desvio: {}".format(len(l1), e1, ds1)

    l2 = simulate_niters(iters=1000, N=5, S=2, TR=0.125, TF=1.0)
    e2 = esperanza(iters=len(l2), lista=l2)
    ds2 = desvio(iters=len(l2), lista=l2, esperanza=e2)
    print "En {} iteraciones con S=2, OP=1, Esperanza: {}, Desvio: {}".format(len(l2), e2, ds2)

    l3 = simulate_niters(iters=10000, N=5, S=2, TR=0.125, TF=1.0)
    e3 = esperanza(iters=len(l3), lista=l3)
    ds3 = desvio(iters=len(l3), lista=l3, esperanza=e3)
    print "En {} iteraciones con S=2, OP=1, Esperanza: {}, Desvio: {}".format(len(l3), e3, ds3)

    l4 = simulate_niters(iters=100, N=5, S=3, TR=0.125, TF=1.0)
    e4 = esperanza(iters=len(l4), lista=l4)
    ds4 = desvio(iters=len(l4), lista=l4, esperanza=e4)
    print "En {} iteraciones con S=3, OP=1, Esperanza: {}, Desvio: {}".format(len(l4), e4, ds4)

    l5 = simulate_niters(iters=1000, N=5, S=3, TR=0.125, TF=1.0)
    e5 = esperanza(iters=len(l5), lista=l5)
    ds5 = desvio(iters=len(l5), lista=l5, esperanza=e5)
    print "En {} iteraciones con S=3, OP=1, Esperanza: {}, Desvio: {}".format(len(l5), e5, ds5)

    l6 = simulate_niters(iters=10000, N=5, S=3, TR=0.125, TF=1.0)
    e6 = esperanza(iters=len(l6), lista=l6)
    ds6 = desvio(iters=len(l6), lista=l6, esperanza=e6)
    print "En {} iteraciones con S=3, OP=1, Esperanza: {}, Desvio: {}".format(len(l6), e6, ds6)
