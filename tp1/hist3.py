import math
import random
import matplotlib.pyplot as plt
import ejercicio2

x = ejercicio2.simulate_niters(iters=10000, N=5, S=2, TR=0.125, TF=1.0, OP=2)

n, bins, patches = plt.hist(
    x, bins=50, range=(0, 15),
    color='red', label="S=2, OP=2", normed=True)

plt.xlabel('Tiempo')
plt.ylabel('Frecuencia normalizada')
plt.title(r'Histograma de Tiempos de Falla en 10000 simulaciones')
plt.legend()
plt.show()
