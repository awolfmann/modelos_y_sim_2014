# Poisson no homogeneo, adelgazamiento simple 

import math
import random

def int_func(t):
    x = 3.0 + (4.0 / (t + 1.0))
    return x

def experimento(lamda, T):
    # lamda: Maximo valor de int_func(t) con 0 <= t <= 10

    t = 0.0 # Tiempo actual
    I = 0.0 # Numero de eventos
    s = []  #Tiempos de llegada de los eventos
    
    count = 0 # contar eficiencia
    while True:
        u = random.random()
        if (t - (1.0 / lamda) * math.log(u)) > T:
            break
        else:
            t = t - (1.0 / lamda) * math.log(u)
            v = random.random()
            l = int_func(t) / lamda
            if v < l:
                I += 1.0
                s.append(t)
        count += 1

    return count 
    #return s
def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento(7.0 , 10.0)

    prom = s/n
    return prom

#print experimento(7.0 , 10.0)
print esp(1000)
