# Poisson no homogeneo, adelgazamiento mejorado 

import math
import random

def int_func(t):
    x = 3.0 + (4.0 / (t + 1.0))
    return x

def experimento(t_list, T):

    t = 0.0
    N = 0.0
    s = []
    lamda = [int_func(t_list[i]) for i in range(len(t_list))]
    count = 0 # contar eficiencia

    while True:
        u = random.random()
        t = t + (1.0 / lamda[j]) * math.log(u)
        if t > T:
            break
        v = random.random()
        
        l = int_func(t) / lamda[j]
        if v < l:
            N += 1.0
            s.append(t)
        count += 1
        print count

    #return count 
    return s


print experimento(t_list=[0.0, 4.0, 10.0], T=10.0)

    def run_enhanced(self, partition, lambdas, limit):
        t = 0
        j = 1
        i = 0
        s = {}

        while True:
            u = random()
            x = (-1.0 / lambdas[j]) * math.log(u)

            while t + x > partition[j]:
                if j == limit:
                    return [i, s]
                x = (x - partition[j] + t) * lambdas[j] / lambdas[j + 1]
                t = partition[j]
                j += 1

            t = t + x
            v = random()
            if v < self.i(t) / lambdas[j]:
                i += 1
                s[i] = t

        return [i, s]
