# Poisson no homogeneo, adelgazamiento mejorado 

import math
import random

def int_func(t):
    x = 3.0 + (4.0 / (t + 1.0))
    return x

def experimento(t_list):

    t = 0.0
    I = 0.0
    s = []
    j = 0 
    lamda = [int_func(t_list[i]) for i in range(len(t_list))]
    count = 0 # contar eficiencia

    #while True:
    while t < 10.0:
        u = random.random()
        x = (t - (1.0 / lamda[j]) * math.log(u))

        
        while t + x > t_list[j]:

            if j == len(t_list) - 1:
                break
                #return count
                #return s

            x = (x - t_list[j] + t) * (lamda[j] / lamda[j+1])           
            t = t_list[j]
            j += 1
            
        t = t + x
        v = random.random()
        l = int_func(t) / lamda[j]
        if v < l:
            I += 1.0
            s.append(t)
        count += 1
        #print count

    #return count 
    return s

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento(t_list=[0.0, 4.0, 10.0])

    prom = s/n
    return prom

print experimento(t_list=[0.0, 4.0, 10.0])
#print esp(1000)
