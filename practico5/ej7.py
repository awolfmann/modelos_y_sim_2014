# Rechazo de una gamma con una exponencial 
import math
import random

def experimento():
    v = random.random()
    y = -2.0 * math.log(v)
    alfa = 2.0
    beta = 1.0
    fy = y**(alfa - 1.0) * math.exp(-y * beta)
    lamda = 0.5
    gy = lamda * math.exp(-lamda * y)
    c = 4.0 / math.e
    count = 1
    u = random.random()
    while u > fy / (c * gy): 
        u = random.random()
        y = -2.0 * math.log(u)
        fy = y**(alfa - 1.0) * math.exp(-y * beta)
        gy = lamda * math.exp(-lamda * y)
        count += 1

    return y

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento()

    prom = s/n
    return prom

print esp(1000000)

