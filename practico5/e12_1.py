""" Algoritmo de adelgazamiento para procesos poisson no homogeneos """
import random
import math


# Funcion de intensidad del proceso poisson no homogeneo
def lambda_f(t):
    return 3 + (4 / float(t + 1))


# Algoritmo de adelgazamiento simple
def adelgazamiento(T):
    t = 0  # Tiempo actual
    I = 0  # Numero de eventos
    S = []  # Tiempos de llegada de los eventos
    l = 7  # Maximo valor de lambda_f(t) con 0 <= t <= 10
    while True:
        U = random.random()
        if t - (1 / float(l)) * math.log(U) > T:
            break
        else:
            t = t - (1 / float(l)) * math.log(U)
            V = random.random()
            if V < lambda_f(t) / float(l):
                I += 1
                S.append(t)
    return I


# Algoritmo de adelgazamiento mejorado con 10 subintervalos
def adelgazamiento2(T):
    t = 0
    k = 10
    J = 1
    I = 0
    S = []
    l_vector = [lambda_f(i) for i in range(k + 1)]
    while True:
        U = random.random()
        X = - (1 / float(l_vector[J - 1])) * math.log(U)
        while t + X > J:
            if J == k + 1:
                return I
            X = (X - J + t) * (l_vector[J - 1] / float(l_vector[J]))
            t = J
            J = J + 1
        t = t + X
        V = random.random()
        if V < lambda_f(t) / float(l_vector[J - 1]):
            I += 1
            S.append(t)


# Comparo la media muestral de eventos generados en cada algoritmo
def prueba(steps, T):
    I1 = 0
    I2 = 0
    for i in range(steps):
        I1 += adelgazamiento(T)
        I2 += adelgazamiento2(T)
    print "Media de eventos generados, primer algoritmo = ", I1 / float(steps)
    print "Media de eventos generados, segundo algoritmo = ", I2 / float(steps)
