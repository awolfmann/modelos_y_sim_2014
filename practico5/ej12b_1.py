# Poisson no homogeneo, adelgazamiento mejorado 

import math
import random

def int_func(t):
    x = 3.0 + (4.0 / (t + 1.0))
    return x

def experimento():

    t = 0.0
    I = 0.0
    s = []
    j = 1 
    k = 10
    lamda = [int_func(i) for i in range(k+1)]
    count = 0 # contar eficiencia

    while True:
        u = random.random()
        x = (t - (1.0 / float(lamda[j-1])) * math.log(u))
        while t + x > j:
            if j == k + 1:
                break
            x = (x - j + t) * (float(lamda[j-1]) / float(lamda[j]))           
            t = j
            j += 1
            
        t = t + x
        v = random.random()
        l = int_func(t) / float(lamda[j-1])
        if v < l:
            I += 1.0
            s.append(t)
        count += 1

    #return count 
    return s


print experimento()
