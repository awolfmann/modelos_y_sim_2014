# Generar las primeras T unidades de tiempo de un proceso de Poisson, 
# parametro lamda

import math
import random

def experimento(lamda, T):
    t = 0.0
    I = 0.0
    s = []
    total_psj = 0.0

    while True:
        u = random.random()
        if (t - (1.0 / lamda) * math.log(u)) > T :
            break
        else:
            psj = 20.0 + int(21 * random.random())  
            t = t - (1.0 / lamda) * math.log(u)
            I += 1
            s.append((t, psj))
            total_psj += psj

    return total_psj


def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento(5.0, 1.0)

    prom = s/n

    return prom

#print experimento(5.0, 1.0)

print "esperanza ", esp(10000)
