#Generar v.a. Gamma, parametros n(alfa), lamda(beta)
import math
import random
import operator


def experimento(n,lamda):
    u = []
    for i in range(n):
        ui = random.random()
        u.append(ui)

    prod = reduce(operator.mul, u, 1)
        
    x = - (1.0/lamda) * math.log(prod)
    
    return x


