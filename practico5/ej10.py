# Generar las primeras T unidades de tiempo de un proceso de Poisson, 
# parametro lamda

import math
import random

def experimento(lamda, T):
    t = 0.0
    I = 0.0
    s = []

    while True:
        u = random.random()
        if (t - (1.0 / lamda) * math.log(u)) > T :
            break
        else:
            t = t - (1.0 / lamda) * math.log(u)
            I += 1
            s.append(t)

    #return s
    return I

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento(3.0, 4.0)

    prom = s/n

    return prom


print "esperanza ", esp(10000)
