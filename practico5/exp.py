#Generar v.a. Exponencial, parametro lamda
import math
import random

def experimento(lamda):
    u = random.random()
    x = - (1.0/lamda) * math.log(u)
    
    return x
