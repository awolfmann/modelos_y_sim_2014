# Generar variable aleatoria normal mediante generacion de exponenciales

import math
import random

def experimento():

    while True:
        u1 = random.random()
        y1 = - math.log(u1)
        u2 = random.random()
        y2 = - math.log(u2)
        
        y3 = y2 - (math.pow((y1 - 1.0), 2.0) / 2.0)
    
        if (y3 > 0.0):
            y = y3 
            break

    u = random.random()
    if u <= 0.5:
        z = y1
    else:
        z = -y1
    
    #print "z ", z
    return z


def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento()

    prom = s/n

    return prom


def var(n):
    #estimador varianza, varianza muestral
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento() ** 2.0 

    var = (s / n) - (esp(n) ** 2.0)

    return var

print "media ", esp(10000)
print "varianza ", var(10000) 
