# Generar variable aleatoria normal mediante metodo polar

import math
import random

def experimento():
    u1 = random.random()
    u2 = random.random()    

    x = math.sqrt(-2.0 * math.log(u1)) * math.cos( 2.0 * math.pi * u2)
#    y = math.sqrt(-2.0 * math.log(u1)) * math.sin( 2.0 * math.pi * u2)
    
    return x

#def experimento1():
# Eficiente
#    x = 0.0
#    while True:
#        u1 = random.random()
#        u2 = random.random()
#        v1 = 2.0 * u1 - 1.0
#        v2 = 2.0 * u2 - 1.0
#        s = v1 ** 2.0 + v2 ** 2.0
#        if s < 1:
#            break

#        x = math.sqrt( ((-2.0 * math.log(s)) / 2.0) * v1 )
#        # y = math.sqrt( ((-2.0 * math.log(s)) / 2.0) * v2 )

#    print "x ", x
#    return x 

def esp(n):
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento()

    prom = s/n

    return prom


def var(n):
    #estimador varianza, varianza muestral
    s = 0.0
    prom = 0.0
    for i in range(n):
        s += experimento() ** 2.0 

    var = (s / n) - (esp(n) ** 2.0)

    return var

print "media ", esp(10000)
print "varianza ", var(10000)
